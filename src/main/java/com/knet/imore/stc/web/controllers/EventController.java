package com.knet.imore.stc.web.controllers;

import com.knet.imore.stc.common.dto.EventDTO;
import com.knet.imore.stc.service.services.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;

@RestController
@RequestMapping(value = "/event")
public class EventController {

    @Autowired
    private EventService eventService;

    @PostMapping
    public void event(@RequestBody EventDTO event){
        eventService.processEvent(event);
    }
}
