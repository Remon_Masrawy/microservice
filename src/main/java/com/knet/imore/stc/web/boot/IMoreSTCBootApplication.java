package com.knet.imore.stc.web.boot;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.security.servlet.ManagementWebSecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.env.RandomValuePropertySource;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.net.InetAddress;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

@Slf4j
@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = {"com.knet.imore.stc.*"},
        exclude = { SecurityAutoConfiguration.class, ManagementWebSecurityAutoConfiguration.class})
public class IMoreSTCBootApplication{

    public static void main(String[] args){
        log.info("********** Read Main Method **********");
        addManagementSecurityCredentials();
        addCustomPlatformProperties();
        trustAllCertificates();
        SpringApplication.run(IMoreSTCBootApplication.class);
    }

    public static void addCustomPlatformProperties() {
        try {
            System.setProperty("stc.host", InetAddress.getLocalHost().getCanonicalHostName());
        } catch (Exception e) {
        }
    }

    public static void addManagementSecurityCredentials() {
        try {
            RandomValuePropertySource randomValuePropertySource = new RandomValuePropertySource("random");
            System.setProperty("managementUsername", randomValuePropertySource.getProperty("random.value").toString());
            System.setProperty("managementPassword", randomValuePropertySource.getProperty("random.value").toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void trustAllCertificates(){
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            @Override
            public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {}
            @Override
            public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {}
            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }
        }};
        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            ;
        }
    }
}
