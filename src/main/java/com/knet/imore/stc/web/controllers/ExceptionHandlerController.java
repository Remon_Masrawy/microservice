package com.knet.imore.stc.web.controllers;

import com.knet.imore.stc.common.exceptions.RequestParamNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionHandlerController {

    @ExceptionHandler(RequestParamNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public RequestParamNotFoundException requestParamNotFoundExceptionHandler(RequestParamNotFoundException ex){
        return ex;
    }
}
