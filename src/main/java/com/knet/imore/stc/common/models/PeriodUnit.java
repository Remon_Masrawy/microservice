package com.knet.imore.stc.common.models;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum PeriodUnit implements StringEnum {

    HOUR("hour"), DAY("day"), MONTH("month"), YEAR("year");

    private String value;
    private PeriodUnit(String value){
        this.value = value;
    }

    @Override
    public String getValue() {
        return value;
    }
    @Override
    public void setValue(String value) {
        this.value = value;
    }

    @JsonCreator
    public static PeriodUnit convert(String key){
        return Convertible.convert(values(), key);
    }
}
