package com.knet.imore.stc.common.dto.pn;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class BasicFullModel extends BasicSimpleModel {

    private String primaryName;

    private String secondaryName;

    private boolean active;
}
