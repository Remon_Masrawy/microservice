package com.knet.imore.stc.common.dto;

import java.util.Map;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class QuantifiableItemDTO extends AbstractDTO {

    private String name;
    private String description;
    private UnitDTO unit;
    private Map<String, String> metaData;
}
