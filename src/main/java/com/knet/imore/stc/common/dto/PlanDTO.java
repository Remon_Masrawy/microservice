package com.knet.imore.stc.common.dto;

import java.util.Map;
import java.util.Set;
import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonProperty;

import com.knet.imore.stc.common.models.PlanType;


@Getter
@Setter
public class PlanDTO extends AbstractDTO {

    private Long id;
    private String name;
    private String description;
    @JsonProperty("initial_price")
    private PriceDTO initialPrice;
    @JsonProperty("plan_items")
    private Set<PlanItemDTO> planItems;
    private Set<FeatureDTO> features;
    private Set<PriceDTO> prices;
    @JsonProperty("meta_data")
    private Map<String, String> metaData;
    private LocalDateTime created;
    private LocalDateTime modified;
    private PlanType type;
    @JsonProperty("maximum_allowed_subscriptions")
    private String maximumAllowedSubscriptions;
}
