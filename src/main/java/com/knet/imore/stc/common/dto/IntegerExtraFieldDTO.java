package com.knet.imore.stc.common.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IntegerExtraFieldDTO extends ExtraFieldDTO {

    private Long value;
}
