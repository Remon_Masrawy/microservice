package com.knet.imore.stc.common.models;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum PlanType implements StringEnum {
    BASE("base"),
    ADDON("addon");

    private String value;
    private PlanType(String value){
        this.value = value;
    }

    @Override
    public String getValue() {
        return value;
    }
    @Override
    public void setValue(String value) {
        this.value = value;
    }

    @JsonCreator
    public static PlanType convert(String key){
        return Convertible.convert(values(), key);
    }
}
