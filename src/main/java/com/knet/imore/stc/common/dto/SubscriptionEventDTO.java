package com.knet.imore.stc.common.dto;

import lombok.Getter;
import lombok.Setter;
import com.fasterxml.jackson.annotation.JsonProperty;

@Setter
@Getter
public class SubscriptionEventDTO extends EventDTO {

    @JsonProperty("data")
    private SubscriptionDTO subscriptionDTO;
}
