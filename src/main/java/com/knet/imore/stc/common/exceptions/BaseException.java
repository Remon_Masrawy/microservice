package com.knet.imore.stc.common.exceptions;

public interface BaseException {
    public int getErrorCode();
}
