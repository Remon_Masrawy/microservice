package com.knet.imore.stc.common.exceptions;

import com.knet.imore.stc.common.models.ErrorConstant;

public class EmptySecretKeyException extends BaseRuntimeException {

    private static final String DEFAULT_MSG = "Error: empty secret key exception.";

    public EmptySecretKeyException(){
        super(DEFAULT_MSG);
    }

    @Override
    public int getErrorCode() {
        return ErrorConstant.EMPTY_SECRET_KEY_ERROR_CODE;
    }
}
