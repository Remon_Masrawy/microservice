package com.knet.imore.stc.common.exceptions;

import com.knet.imore.stc.common.models.ErrorConstant;

public class CustomerUpdateException extends BaseRuntimeException {

    private static final String DEFAULT_MSG = "Error: cannot update customer exception.";

    public CustomerUpdateException(){
        super(DEFAULT_MSG);
    }

    @Override
    public int getErrorCode() {
        return ErrorConstant.CUSTOMER_UPDATE_ERROR_CODE;
    }
}
