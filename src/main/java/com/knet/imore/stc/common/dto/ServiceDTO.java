package com.knet.imore.stc.common.dto;

import java.util.Map;
import java.util.Set;
import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonProperty;

import com.knet.imore.stc.common.models.ServiceStatus;

@Getter
@Setter
public class ServiceDTO extends AbstractDTO {

    private Long id;
    private ServiceStatus status;
    private String name;
    private String description;
    private String url;
    private Set<ItemDTO> items;
    private Set<FeatureDTO> features;
    private Set<PlanDTO> plans;
    //bundles: object[] Bundle
    @JsonProperty("meta_data")
    private Map<String, String> metaData;
    private LocalDateTime created;
    private LocalDateTime modified;
}
