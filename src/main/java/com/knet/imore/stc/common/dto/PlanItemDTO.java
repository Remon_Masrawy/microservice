package com.knet.imore.stc.common.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PlanItemDTO extends AbstractDTO {

    private Long id;
    private ItemDTO item;
    private UnitDTO unit;
    private Double quantity;
}
