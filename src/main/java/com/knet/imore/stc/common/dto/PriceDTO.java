package com.knet.imore.stc.common.dto;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonProperty;

import com.knet.imore.stc.common.models.PeriodUnit;
import com.knet.imore.stc.common.models.PriceType;


@Getter
@Setter
public class PriceDTO extends AbstractDTO {

    private Long id;
    private PriceType type;
    private Integer period;
    @JsonProperty("period_unit")
    private PeriodUnit periodUnit;
    private Double amount;
    private String currency;
    private LocalDateTime created;
    private LocalDateTime modified;
}
