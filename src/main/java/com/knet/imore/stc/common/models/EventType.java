package com.knet.imore.stc.common.models;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum EventType implements StringEnum{
    SUBSCRIPTION_CREATED("subscription.created"),
    SUBSCRIPTION_SUSPENDED("subscription.suspended"),
    SUBSCRIPTION_RESUMED("subscription.resumed"),
    SUBSCRIPTION_CANCELED("subscription.canceled"),
    SUBSCRIPTION_PLAN_CHANGED("subscription.plan.changed"),
    SUBSCRIPTION_ADDON_ATTACHED("subscription.addon.attached"),
    SUBSCRIPTION_ADDON_CANCELED("subscription.addon.canceled"),

    SUBSCRIPTION_USER_ADDED("subscription.user.added"),
    SUBSCRIPTION_USER_REMOVED("subscription.user.removed"),
    SUBSCRIPTION_USER_ROLES_CHANGED("subscription.user.roles.changed"),

    ACCOUNT_SUSPENDED("account.suspended"),
    ACCOUNT_RESUMED("account.resumed"),
    ACCOUNT_TRIAL_FIRST_SUSPENDED("account.trial.first.suspended"),
    ACCOUNT_TRIAL_SECOND_SUSPENDED("account.trial.second.suspended"),
    ACCOUNT_TRIAL_FINAL_SUSPENDED("account.trial.final.suspended"),
    ACCOUNT_BILLING_ACTIVATED("account.billing.activated"),
    ACCOUNT_TERMINATED("account.terminated"),

    WEBHOOK_TEST("webhook.test"),

    EVENT_EXPIRED("event.expired");

    private String value;

    private EventType(String value){
        this.value = value;
    }

    @Override
    public String getValue() {
        return value;
    }
    @Override
    public void setValue(String value) {
        this.value = value;
    }
    @JsonCreator
    public static EventType convert(String key){
        return Convertible.convert(values(), key);
    }
}
