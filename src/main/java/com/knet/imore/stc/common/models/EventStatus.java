package com.knet.imore.stc.common.models;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum EventStatus implements StringEnum {

    SUCCESS("success"),
    ERROR("error"),
    REJECT("reject");

    private String value;

    private EventStatus(String value){
        this.value = value;
    }

    @Override
    public String getValue() {
        return value;
    }
    @Override
    public void setValue(String value) {
        this.value = value;
    }

    @JsonCreator
    public static EventStatus convert(String key){
        return Convertible.convert(values(), key);
    }
}
