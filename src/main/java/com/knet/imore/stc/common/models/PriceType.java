package com.knet.imore.stc.common.models;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum PriceType implements StringEnum {

    ONE_TIME("one_time"), RECURRING("recurring");

    private String value;
    private PriceType(String value){
        this.value = value;
    }

    @Override
    public String getValue() {
        return value;
    }
    @Override
    public void setValue(String value) {
        this.value = value;
    }

    @JsonCreator
    public static PriceType convert(String key){
        return Convertible.convert(values(), key);
    }
}
