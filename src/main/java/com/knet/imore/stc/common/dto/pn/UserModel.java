package com.knet.imore.stc.common.dto.pn;

import com.knet.imore.stc.common.models.Gender;
import lombok.Getter;
import lombok.Setter;
import com.knet.imore.stc.common.dto.AbstractDTO;
import com.knet.imore.stc.common.models.UserType;

@Setter
@Getter
public class UserModel extends BasicModel {

    private Long providerId;
    private UserType type;
    private String firstName;
    private String lastName;
    private String username;
    private Gender gender;
    private String email;
    private String userStatus;
    private String mobileNumber;
    private String preferredLanguage;
    private String fullEnglishName;
}
