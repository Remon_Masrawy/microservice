package com.knet.imore.stc.common.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerDTO extends AbstractDTO {
    private Long id;
    private String name;
}
