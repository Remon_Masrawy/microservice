package com.knet.imore.stc.common.dto.pn;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class RoleModel extends BasicModel {

    private String name;
    private List<PermissionModel> permissionList;
}
