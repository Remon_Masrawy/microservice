package com.knet.imore.stc.common.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

@Setter
@Getter
@AllArgsConstructor
public class STCCallbackResponse implements ClientHttpResponse, Serializable {

    private HttpStatus statusCode;
    private String statusText;

    @Override
    public int getRawStatusCode() throws IOException {
        return statusCode.value();
    }

    @Override
    public void close() {

    }

    @Override
    public InputStream getBody() throws IOException {
        return null;
    }

    @Override
    public HttpHeaders getHeaders() {
        return null;
    }
}
