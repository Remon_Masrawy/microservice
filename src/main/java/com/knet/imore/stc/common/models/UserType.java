package com.knet.imore.stc.common.models;

public enum UserType implements StringEnum {

    ORG_ADMIN, NORMAL_USER;

    private String value;
    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setValue(String value) {
        this.value = value;
    }
}
