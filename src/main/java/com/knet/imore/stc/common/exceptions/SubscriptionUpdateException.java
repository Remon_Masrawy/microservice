package com.knet.imore.stc.common.exceptions;

import com.knet.imore.stc.common.models.ErrorConstant;

public class SubscriptionUpdateException extends BaseRuntimeException {

    private static final String DEFAULT_MSG = "Error: cannot update subscription exception.";

    public SubscriptionUpdateException(){
        super(DEFAULT_MSG);
    }

    @Override
    public int getErrorCode() {
        return ErrorConstant.SUBSCRIPTION_UPDATE_ERROR_CODE;
    }
}
