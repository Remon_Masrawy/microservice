package com.knet.imore.stc.common.models;

public enum Gender implements StringEnum {

    MALE("Male"), FEMALE("Female"), UNKNOWN("Unknown");

    private String value;
    private Gender(String value){
        this.value = value;
    }

    @Override
    public String getValue() {
        return value;
    }
    @Override
    public void setValue(String value) {
        this.value = value;
    }
}
