package com.knet.imore.stc.common.dto;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import com.knet.imore.stc.common.models.EventType;

@Setter
@Getter
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type", visible = true)
@JsonSubTypes({
    @JsonSubTypes.Type(value = SubscriptionEventDTO.class, name = "subscription.created"),
    @JsonSubTypes.Type(value = SubscriptionEventDTO.class, name = "subscription.suspended"),
    @JsonSubTypes.Type(value = SubscriptionEventDTO.class, name = "subscription.resumed"),
    @JsonSubTypes.Type(value = SubscriptionEventDTO.class, name = "subscription.canceled"),
    @JsonSubTypes.Type(value = SubscriptionEventDTO.class, name = "subscription.plan.changed"),
    @JsonSubTypes.Type(value = SubscriptionEventDTO.class, name = "subscription.addon.attached"),
    @JsonSubTypes.Type(value = SubscriptionEventDTO.class, name = "subscription.addon.canceled"),
    @JsonSubTypes.Type(value = SubscriptionUserEventDTO.class, name = "subscription.user.added"),
    @JsonSubTypes.Type(value = SubscriptionUserEventDTO.class, name = "subscription.user.removed"),
    @JsonSubTypes.Type(value = SubscriptionUserEventDTO.class, name = "subscription.user.roles.changed"),
    @JsonSubTypes.Type(value = CustomerEventDTO.class, name = "account.suspended"),
    @JsonSubTypes.Type(value = CustomerEventDTO.class, name = "account.resumed"),
    @JsonSubTypes.Type(value = CustomerEventDTO.class, name = "account.trial.first.suspended"),
    @JsonSubTypes.Type(value = CustomerEventDTO.class, name = "account.trial.second.suspended"),
    @JsonSubTypes.Type(value = CustomerEventDTO.class, name = "account.trial.final.suspended"),
    @JsonSubTypes.Type(value = CustomerEventDTO.class, name = "account.billing.activated"),
    @JsonSubTypes.Type(value = CustomerEventDTO.class, name = "account.terminated"),
    @JsonSubTypes.Type(value = TestEventDTO.class, name = "webhook.test"),
    @JsonSubTypes.Type(value = ExpiredEventDTO.class, name = "event.expired")
})
public class EventDTO extends AbstractDTO {

    private String id;
    private EventType type;
    private ServiceDTO service;
    @JsonProperty("created_at")
    private LocalDateTime createdAt;
    @JsonProperty("api_version")
    private String apiVersion;
}
