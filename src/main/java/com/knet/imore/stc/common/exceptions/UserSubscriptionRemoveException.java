package com.knet.imore.stc.common.exceptions;

import com.knet.imore.stc.common.models.ErrorConstant;

public class UserSubscriptionRemoveException extends BaseRuntimeException {

    private static final String DEFAULT_MSG = "Error: cannot remove user from subscription exception.";

    public UserSubscriptionRemoveException(){
        super(DEFAULT_MSG);
    }

    @Override
    public int getErrorCode() {
        return ErrorConstant.CANNOT_REMOVE_USER_FROM_SUBSCRIPTION_TYPE_CODE;
    }
}
