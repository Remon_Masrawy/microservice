package com.knet.imore.stc.common.models;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum SubscriptionStatus implements StringEnum {
    PENDING("pending"),
    ACTIVE("active"),
    ERROR("error"),
    SUSPENDED("suspended"),
    CANCELED("canceled");

    private String value;
    private SubscriptionStatus(String value){
        this.value = value;
    }
    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setValue(String value) {
        this.value = value;
    }

    @JsonCreator
    public static SubscriptionStatus convert(String key){
        return Convertible.convert(values(), key);
    }
}
