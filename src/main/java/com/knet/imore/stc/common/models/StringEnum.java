package com.knet.imore.stc.common.models;

public interface StringEnum {
    public String getValue();
    public void setValue(String value);
}
