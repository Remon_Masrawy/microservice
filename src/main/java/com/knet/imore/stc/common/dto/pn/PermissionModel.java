package com.knet.imore.stc.common.dto.pn;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class PermissionModel extends BasicModel {
    private String name;
}
