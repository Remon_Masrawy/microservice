package com.knet.imore.stc.common.exceptions;

import com.knet.imore.stc.common.models.ErrorConstant;

public class UnknownEventEngineException extends BaseRuntimeException {

    private static final String DEFAULT_MSG = "Error: event come from unknown source exception.";

    public UnknownEventEngineException(){
        super(DEFAULT_MSG);
    }

    @Override
    public int getErrorCode() {
        return ErrorConstant.UNKNOWN_EVENT_ENGINE_ERROR_CODE;
    }
}
