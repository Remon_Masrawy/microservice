package com.knet.imore.stc.common.exceptions;

import com.knet.imore.stc.common.models.ErrorConstant;

public class UserSubscriptionAdditionException extends BaseRuntimeException {

    private static final String DEFAULT_MSG = "Error: cannot add user to subscription exception.";

    public UserSubscriptionAdditionException(){
        super(DEFAULT_MSG);
    }

    @Override
    public int getErrorCode() {
        return ErrorConstant.CANNOT_ADD_USER_TO_SUBSCRIPTION_TYPE_CODE;
    }
}
