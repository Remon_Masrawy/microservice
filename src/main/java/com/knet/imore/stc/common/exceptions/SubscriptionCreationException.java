package com.knet.imore.stc.common.exceptions;

import com.knet.imore.stc.common.models.ErrorConstant;

public class SubscriptionCreationException extends BaseRuntimeException {

    private static final String DEFAULT_MSG = "Error: cannot create subscription exception.";

    public SubscriptionCreationException(){
        super(DEFAULT_MSG);
    }

    @Override
    public int getErrorCode() {
        return ErrorConstant.SUBSCRIPTION_CREATION_ERROR_CODE;
    }
}
