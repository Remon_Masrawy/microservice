package com.knet.imore.stc.common.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BooleanExtraFieldDTO extends ExtraFieldDTO {

    private Boolean value;
}
