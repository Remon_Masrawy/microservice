package com.knet.imore.stc.common.dto;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonProperty;

@Setter
@Getter
public class SubscriptionUserEventDTO extends EventDTO {

    @JsonProperty("data")
    private SubscriptionUserDTO subscriptionUserDTO;
}
