package com.knet.imore.stc.common.models;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum ServiceStatus implements StringEnum{

    DRAFT("draft"), PENDING("pending"), SANDBOX("sandbox"),
    PUBLISHED("published"), REJECTED("rejected"), WITHDRAWN("withdrawn");

    private String value;
    private ServiceStatus(String value){
        this.value = value;
    }

    @Override
    public String getValue() {
        return value;
    }
    @Override
    public void setValue(String value) {
        this.value = value;
    }

    @JsonCreator
    public static ServiceStatus convert(String key){
        return Convertible.convert(values(), key);
    }
}
