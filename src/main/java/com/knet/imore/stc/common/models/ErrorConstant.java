package com.knet.imore.stc.common.models;

public class ErrorConstant {

    public static final int EMPTY_EVENT_BODY_ERROR_CODE                 = 100;
    public static final int EMPTY_SECRET_KEY_ERROR_CODE                 = 101;
    public static final int REQUEST_PARAM_NOT_FOUND_ERROR_CODE          = 102;
    public static final int UNKNOWN_EVENT_ENGINE_ERROR_CODE             = 103;
    public static final int UNSUPPORTED_ENUM_TYPE_CODE                  = 104;
    public static final int SUBSCRIPTION_CREATION_ERROR_CODE            = 105;
    public static final int SUBSCRIPTION_EXPIRE_ERROR_CODE            = 105;
    public static final int SUBSCRIPTION_UPDATE_ERROR_CODE              = 105;
    public static final int CUSTOMER_UPDATE_ERROR_CODE                  = 105;
    public static final int CANNOT_ADD_USER_TO_SUBSCRIPTION_TYPE_CODE   = 106;
    public static final int CANNOT_REMOVE_USER_FROM_SUBSCRIPTION_TYPE_CODE = 107;
    public static final int CANNOT_UPDATE_USER_ROLES_TYPE_CODE          = 108;


    public static final String INTERNAL_SERVER_ERROR = "Internal server error occurred.";
    public static final String SUCCESS = "Operation done successfully.";
    public static final String SUBSCRIPTION_USER_ADDED_SUCCESSFULLY = "User added to subscription successfully.";
}
