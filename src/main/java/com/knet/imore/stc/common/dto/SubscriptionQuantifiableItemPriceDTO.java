package com.knet.imore.stc.common.dto;

import lombok.Getter;
import lombok.Setter;
import com.fasterxml.jackson.annotation.JsonProperty;

@Getter
@Setter
public class SubscriptionQuantifiableItemPriceDTO extends AbstractDTO {

    @JsonProperty("quantifiable_item_price")
    private QuantifiableItemPriceDTO quantifiableItemPrice;
    private Integer quantity;
}
