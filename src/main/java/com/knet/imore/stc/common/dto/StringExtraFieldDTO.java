package com.knet.imore.stc.common.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StringExtraFieldDTO extends ExtraFieldDTO {

    private String value;
}
