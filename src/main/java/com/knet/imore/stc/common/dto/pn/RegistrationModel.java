package com.knet.imore.stc.common.dto.pn;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class RegistrationModel extends BasicModel {

    private CompanyFullModel companyModel;
    private SubscriptionModel SubscriptionModel;
    private UserModel adminUser;
    private UserModel endUser;
    private String primaryLanguage;
    private int noOfUsers;
    private String context;
}
