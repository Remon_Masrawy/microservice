package com.knet.imore.stc.common.dto;

import java.util.Set;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonProperty;

@Getter
@Setter
public class SubscriptionUserDTO extends AbstractDTO {

    private Long id;
    @JsonProperty("subscription")
    private Long subscriptionId;
    @JsonProperty("user")
    private Long userId;
    private Set<String> roles;
    @JsonProperty("user_obj")
    private UserDTO user;
}
