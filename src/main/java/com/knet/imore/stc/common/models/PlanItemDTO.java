package com.knet.imore.stc.common.models;

import com.knet.imore.stc.common.dto.AbstractDTO;
import com.knet.imore.stc.common.dto.ItemDTO;
import com.knet.imore.stc.common.dto.UnitDTO;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PlanItemDTO extends AbstractDTO {

    private Long id;
    private ItemDTO item;
    private UnitDTO unit;
    private Double quantity;
}
