package com.knet.imore.stc.common.models;

public class STCConstant {

    public static final int REPLY_CODE_FAILED = 422;
    public static final int REPLY_CODE_ERROR = 500;
    // HTTP status OK
    public static final int REPLY_CODE_SUCCESS = 200;
    // HTTP status not found
    public static final int REPLY_CODE_NOTFOUND = 404;
}
