package com.knet.imore.stc.common.exceptions;

public abstract class BaseRuntimeException extends RuntimeException implements BaseException {

    public BaseRuntimeException(String message){
        super(message);
    }

    public BaseRuntimeException(String message, Throwable cause){
        super(message, cause);
    }
}
