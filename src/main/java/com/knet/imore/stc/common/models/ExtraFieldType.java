package com.knet.imore.stc.common.models;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum ExtraFieldType implements StringEnum {

    TEXT("Text"),
    PASSWORD("Password"),
    SELECT("Select"),
    CHECKBOX("Checkbox"),
    USER("User");

    private String value;
    private ExtraFieldType(String value){
        this.value = value;
    }

    @Override
    public String getValue() {
        return value;
    }
    @Override
    public void setValue(String value) {
        this.value = value;
    }

    @JsonCreator
    public static ExtraFieldType convert(String key){
        return Convertible.convert(values(), key);
    }
}
