package com.knet.imore.stc.common.dto.pn;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AddressModel extends BasicModel {

    private String primaryAddress;
    private String secondaryAddress;
    private String countryCode;
    private String cityCode;
    private String countryName;
    private String cityName;
    private String districtName;
    private String regionCode;
    private String districtCode;
    private String poBox;
    private String latitude;
    private String longitude;
}
