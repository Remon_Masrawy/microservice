package com.knet.imore.stc.common.exceptions;

import com.knet.imore.stc.common.models.ErrorConstant;

public class UserSubscriptionUpdateRolesException extends BaseRuntimeException {

    private static final String DEFAULT_MSG = "Error: cannot update user roles exception.";

    public UserSubscriptionUpdateRolesException(){
        super(DEFAULT_MSG);
    }

    @Override
    public int getErrorCode() {
        return ErrorConstant.CANNOT_UPDATE_USER_ROLES_TYPE_CODE;
    }
}
