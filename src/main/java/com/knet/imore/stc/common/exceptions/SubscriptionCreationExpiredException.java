package com.knet.imore.stc.common.exceptions;

import com.knet.imore.stc.common.models.ErrorConstant;

public class SubscriptionCreationExpiredException extends BaseRuntimeException {

    private static final String DEFAULT_MSG = "Error: cannot expire subscription exception.";

    public SubscriptionCreationExpiredException(){
        super(DEFAULT_MSG);
    }

    @Override
    public int getErrorCode() {
        return ErrorConstant.SUBSCRIPTION_EXPIRE_ERROR_CODE;
    }
}
