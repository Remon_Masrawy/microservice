package com.knet.imore.stc.common.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.knet.imore.stc.common.models.EventStatus;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EventCallbackDTO extends AbstractDTO {

    private EventStatus status;
    private String message;
    @JsonProperty("ref_number")
    private String refNumber;
    @JsonProperty("landing_page_url")
    private String landingPageURL;
    @JsonProperty("management_page_url")
    private String managementPageUrl;
    private String instructions;
}
