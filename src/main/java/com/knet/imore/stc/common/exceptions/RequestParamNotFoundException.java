package com.knet.imore.stc.common.exceptions;

import com.knet.imore.stc.common.models.ErrorConstant;

public class RequestParamNotFoundException extends BaseRuntimeException {

    private static final String DEFAULT_MSG = "Error: request parameter /'X-Cartwheel-Signature/' not found exception.";

    public RequestParamNotFoundException(){
        super(DEFAULT_MSG);
    }

    @Override
    public int getErrorCode() {
        return ErrorConstant.REQUEST_PARAM_NOT_FOUND_ERROR_CODE;
    }
}
