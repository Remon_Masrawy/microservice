package com.knet.imore.stc.common.dto.pn;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ContactInfoModel extends BasicModel {

    private String email;
    private String mobileNo;
    private String mobileNo2;
    private String phoneNo1;
    private String phoneNo2;
    private String faxNo;
    private String webSite;
    private String contactName;
    private String areaCode;
}
