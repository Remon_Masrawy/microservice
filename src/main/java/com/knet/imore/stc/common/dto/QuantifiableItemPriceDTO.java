package com.knet.imore.stc.common.dto;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonProperty;

@Getter
@Setter
public class QuantifiableItemPriceDTO extends AbstractDTO {

    @JsonProperty("quantifiable_item")
    private QuantifiableItemDTO quantifiableItem;
    private String currency;
    private Double price;
    private Integer min;
    private Integer max;
}
