package com.knet.imore.stc.common.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.knet.imore.stc.common.exceptions.UnsupportedEnumTypeException;

import java.util.stream.Stream;

public interface Convertible {

    @JsonCreator
    public static <T extends StringEnum> T convert(T[] ts, String key){
        return Stream.of(ts)
                .filter(t -> t.getValue().equals(key))
                .findFirst()
                .orElseThrow(() -> new UnsupportedEnumTypeException(ts.getClass().getComponentType(), key));
    }
}
