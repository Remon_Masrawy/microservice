package com.knet.imore.stc.common.dto.pn;

import lombok.Getter;
import lombok.Setter;
import com.knet.imore.stc.common.models.EventType;
import com.knet.imore.stc.common.models.PeriodUnit;

@Setter
@Getter
public class SubscriptionModel extends BasicModel {

    private String countryCode;
    private String subscriptionPlan;
    private PeriodUnit paymentTerm;
    private Integer noOfUnits;
    private String expiryDate;
    private Integer organizationId;
    private String userId;
    private String userName;
    private String companyName;
    private String email;
    private Long subscriptionId;
    private EventType status;
    private boolean active;
    private String organizationName;
}
