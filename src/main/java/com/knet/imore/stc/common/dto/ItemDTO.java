package com.knet.imore.stc.common.dto;

import java.util.Map;
import java.util.Set;
import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonProperty;


@Getter
@Setter
public class ItemDTO extends AbstractDTO{

    private Long id;
    private String name;
    private String description;
    private LocalDateTime created;
    private LocalDateTime modified;
    private Set<UnitDTO> units;
    @JsonProperty("meta_data")
    private Map<String, String> metaData;
}
