package com.knet.imore.stc.common.models;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum SubscriptionCancelReason implements StringEnum {

    CUSTOMER_REQUEST("customer_request"),
    PLAN_CHANGE("plan_change"),
    PAST_DUES("past_dues"),
    EXPIRED ("expired"),
    REJECT ("reject");

    private String value;
    private SubscriptionCancelReason(String value){
        this.value = value;
    }

    @Override
    public String getValue() {
        return value;
    }
    @Override
    public void setValue(String value) {
        this.value = value;
    }

    @JsonCreator
    public static SubscriptionCancelReason convert(String key){
        return Convertible.convert(values(), key);
    }
}
