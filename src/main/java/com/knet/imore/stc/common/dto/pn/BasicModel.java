package com.knet.imore.stc.common.dto.pn;

import com.knet.imore.stc.common.dto.AbstractDTO;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class BasicModel implements Serializable {

    private long id;
    private int replyCode = 200;
    private String replyMessage = "message.success";
    /**
     * payload associated with reply message localization
     */
    private transient List<String> payLoadList = new ArrayList<String>();
    private String payload = "";
}
