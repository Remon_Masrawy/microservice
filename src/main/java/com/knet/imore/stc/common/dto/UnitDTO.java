package com.knet.imore.stc.common.dto;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonProperty;

@Getter
@Setter
public class UnitDTO extends AbstractDTO {

    private String id;
    private String name;
    @JsonProperty("short_name")
    private String shortName;
    private String symbol;
}
