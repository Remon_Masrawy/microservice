package com.knet.imore.stc.common.dto.pn;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CompanyFullModel extends BasicFullModel {

    private String primaryShortName;
    private String secondaryShortName;
    private String logo;
    private String financialSystemName;
    private String systemCalendar;
    private String systemVersion;
    private String currencyCode;
    private Long organizationId;
    private AddressModel addressModel;
    private ContactInfoModel contactInfoModel;
    private String adminUnitName;
}
