package com.knet.imore.stc.common.exceptions;

import com.knet.imore.stc.common.models.ErrorConstant;

public class UnsupportedEnumTypeException extends BaseRuntimeException {

    private static final String DEFAULT_MSG = "Error: unsupported enum type exception.";

    public UnsupportedEnumTypeException(){
        super(DEFAULT_MSG);
    }

    public <T> UnsupportedEnumTypeException(Class<T> t, String key){
        super(String.format("Error: cannot find type '%s' in enum '%s'", key, t));
    }

    @Override
    public int getErrorCode() {
        return ErrorConstant.UNSUPPORTED_ENUM_TYPE_CODE;
    }
}
