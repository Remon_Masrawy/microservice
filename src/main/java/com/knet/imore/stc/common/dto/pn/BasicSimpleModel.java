package com.knet.imore.stc.common.dto.pn;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class BasicSimpleModel extends BasicModel {

    private String code;
    private String name;
    private String entityName;
}
