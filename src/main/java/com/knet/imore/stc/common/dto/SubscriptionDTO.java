package com.knet.imore.stc.common.dto;

import java.util.Map;
import java.util.Set;
import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonProperty;

import com.knet.imore.stc.common.models.SubscriptionCancelReason;
import com.knet.imore.stc.common.models.SubscriptionStatus;


@Getter
@Setter
public class SubscriptionDTO extends AbstractDTO {

    private Long id;
    private CustomerDTO customer;
    private PriceDTO price;
    private PlanDTO plan;
    private SubscriptionStatus status;
    private LocalDateTime start;
    @JsonProperty("canceled_at")
    private LocalDateTime canceledAt;
    @JsonProperty("cancel_reason")
    private SubscriptionCancelReason cancelReason;
    private LocalDateTime created;
    @JsonProperty("extra_fields")
    private Map<String, ExtraFieldDTO> extraFields;
    @JsonProperty("base_subscription")
    private Long baseSubscription;
    @JsonProperty("override_price")
    private Double overridePrice;
    @JsonProperty("quantifiable_items_prices")
    private Set<SubscriptionQuantifiableItemPriceDTO> subscriptionQuantifiableItemPrices;
    @JsonProperty("items_price")
    private Double itemsPrice;
    @JsonProperty("end_date")
    private LocalDateTime endDate;
}