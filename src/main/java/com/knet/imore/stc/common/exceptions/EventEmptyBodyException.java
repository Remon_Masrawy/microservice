package com.knet.imore.stc.common.exceptions;

import com.knet.imore.stc.common.models.ErrorConstant;

public class EventEmptyBodyException extends BaseRuntimeException {

    private static final String DEFAULT_MSG = "Error: empty event body exception.";

    public EventEmptyBodyException(){
        super(DEFAULT_MSG);
    }

    @Override
    public int getErrorCode() {
        return ErrorConstant.EMPTY_EVENT_BODY_ERROR_CODE;
    }
}
