package com.knet.imore.stc.common.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ExpiredEventDTO extends EventDTO {

    @JsonProperty("data")
    private EventDTO eventDTO;
}
