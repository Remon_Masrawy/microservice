package com.knet.imore.stc.common.dto;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import com.knet.imore.stc.common.models.ExtraFieldType;

@Setter
@Getter
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = StringExtraFieldDTO.class, name = "Text"),
        @JsonSubTypes.Type(value = StringExtraFieldDTO.class, name = "Password"),
        @JsonSubTypes.Type(value = StringExtraFieldDTO.class, name = "Select"),
        @JsonSubTypes.Type(value = IntegerExtraFieldDTO.class, name = "User"),
        @JsonSubTypes.Type(value = BooleanExtraFieldDTO.class, name = "Checkbox")
})
public abstract class ExtraFieldDTO extends AbstractDTO {

    private ExtraFieldType type;
}
