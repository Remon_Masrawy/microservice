package com.knet.imore.stc.security.configuration;

import com.knet.imore.stc.security.openid.OpenIdConnectFilter;
import com.knet.imore.stc.security.util.Defines;
import com.knet.imore.stc.service.filters.RequestSignatureFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.filter.OAuth2ClientContextFilter;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.springframework.security.web.context.SecurityContextPersistenceFilter;
import org.springframework.web.context.request.RequestContextListener;

@Configuration
@EnableWebSecurity
@EnableOAuth2Client
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private RequestSignatureFilter requestSignatureFilter;

    @Qualifier("openIdOAuth2RestTemplate")
    @Autowired
    private OAuth2RestTemplate restTemplate;

    @Bean(name = "openID")
    public OpenIdConnectFilter openIdConnectFilter() {
        OpenIdConnectFilter openIdFilter = new OpenIdConnectFilter(Defines.ENTRY_POINT_URL);
        openIdFilter.setRestTemplate(restTemplate);
        return openIdFilter;
    }

    @Bean
    public RequestContextListener requestContextListener() {
        return new RequestContextListener();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .httpBasic().disable()
                .formLogin().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS, "**").denyAll()
                .antMatchers(HttpMethod.TRACE, "**").denyAll()
                .antMatchers(HttpMethod.PUT, "**").denyAll()
                .antMatchers(HttpMethod.DELETE, "**").denyAll()
                .antMatchers(HttpMethod.PATCH, "**").denyAll()
                .antMatchers("/ManagementEndPoints/**").permitAll()
                .antMatchers("/event/**").permitAll()
                .antMatchers("/test").permitAll()
                .antMatchers("/oauth2/**").authenticated()
                .antMatchers("/**").denyAll()
                .and()
                .headers()
                .and()
                .sessionManagement().disable()
                .csrf().disable()
                .cors().disable()
                .addFilterAfter(requestSignatureFilter, SecurityContextPersistenceFilter.class)
                .addFilterAfter(new OAuth2ClientContextFilter(), AbstractPreAuthenticatedProcessingFilter.class)
                .addFilterAfter(openIdConnectFilter(), OAuth2ClientContextFilter.class)
                .httpBasic()
                .authenticationEntryPoint(new LoginUrlAuthenticationEntryPoint(Defines.ENTRY_POINT_URL));
    }

    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers(Defines.RESOURCES_PATTERN);
    }

    @Bean
    public OAuth2RestTemplate oAuth2RestTemplate() {
        return new OAuth2RestTemplate(stc());
    }

    @Bean
    @ConfigurationProperties("stc.client")
    public ClientCredentialsResourceDetails stc() {
        return new ClientCredentialsResourceDetails();
    }
}
