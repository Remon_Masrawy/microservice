package com.knet.imore.stc.security.model;

import org.springframework.security.oauth2.common.OAuth2AccessToken;

public class OpenIdModel {

    private String userID;
    private String userName;
    private OAuth2AccessToken accessToken;

    public OpenIdModel(String userID, String userName, OAuth2AccessToken accessToken) {
        this.userID = userID;
        this.userName = userName;
        this.accessToken = accessToken;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public OAuth2AccessToken getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(OAuth2AccessToken accessToken) {
        this.accessToken = accessToken;
    }
}