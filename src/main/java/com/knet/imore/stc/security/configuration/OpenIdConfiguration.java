package com.knet.imore.stc.security.configuration;

import com.knet.imore.stc.security.util.Defines;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.security.oauth2.common.AuthenticationScheme;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;

import java.util.Arrays;

@Configuration
@EnableOAuth2Client
public class OpenIdConfiguration {

    @Value("${stc.clientId}")
    private String clientId;

    @Value("${stc.clientSecret}")
    private String clientSecret;

    @Value("${stc.accessTokenUri}")
    private String accessTokenUri;

    @Value("${stc.userAuthorizationUri}")
    private String userAuthorizationUri;

    @Value("${stc.redirectUri}")
    private String redirectUri;

    @Value("${stc.grantType}")
    private String grantType;

    @Bean
    public OAuth2ProtectedResourceDetails init() {
        AuthorizationCodeResourceDetails openIdDetails = new AuthorizationCodeResourceDetails();
        openIdDetails.setClientId(clientId);
        openIdDetails.setClientSecret(clientSecret);
        openIdDetails.setAccessTokenUri(accessTokenUri);
        openIdDetails.setUserAuthorizationUri(userAuthorizationUri);
        openIdDetails.setPreEstablishedRedirectUri(redirectUri);
        openIdDetails.setGrantType(grantType);
        openIdDetails.setClientAuthenticationScheme(AuthenticationScheme.form);
        openIdDetails.setAuthenticationScheme(AuthenticationScheme.form);
        openIdDetails.setScope(Arrays.asList(Defines.SCOPE_OPEN_ID, Defines.SCOPE_EMAIL, Defines.SCOPE_PROFILE));
        openIdDetails.setUseCurrentUri(false);
        return openIdDetails;
    }

    @Bean (name = "openIdOAuth2RestTemplate")
    public OAuth2RestTemplate openIDTemplate(OAuth2ClientContext oAuth2ClientContext) {
        return new OAuth2RestTemplate(init(), oAuth2ClientContext);
    }
}
