package com.knet.imore.stc.security.controller;

import com.knet.imore.stc.security.model.OpenIdModel;
import com.knet.imore.stc.security.openid.OpenIdConnectUserDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/oauth2")
public class OAuth2Controller {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @RequestMapping(value = "/authenticateStcUsers", method = RequestMethod.GET)
    @ResponseBody
    public OpenIdModel authenticateStcUsers() {
        logger.info("Authenticate stc user");
        OpenIdConnectUserDetails userDetails =
                (OpenIdConnectUserDetails) SecurityContextHolder.getContext().getAuthentication().getDetails();
        OpenIdModel openIdModel = new OpenIdModel(
                userDetails.getUserId(),
                userDetails.getUsername(),
                userDetails.getAccessToken());
        return openIdModel;
    }

}
