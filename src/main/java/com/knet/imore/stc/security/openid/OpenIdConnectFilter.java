package com.knet.imore.stc.security.openid;

import com.auth0.jwk.Jwk;
import com.auth0.jwk.JwkProvider;
import com.auth0.jwk.UrlJwkProvider;
import com.knet.imore.stc.security.model.TokenModel;
import com.knet.imore.stc.security.util.Defines;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.jwt.crypto.sign.RsaVerifier;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URL;
import java.security.interfaces.RSAPublicKey;
import java.util.Date;
import java.util.Enumeration;
import java.util.Map;

public class OpenIdConnectFilter extends AbstractAuthenticationProcessingFilter {

    @Value("${stc.clientId}")
    private String clientId;

    @Value("${stc.issuer}")
    private String issuer;

    @Value("${stc.jwkUrl}")
    private String jwkUrl;

    private OAuth2RestTemplate restTemplate;

    public OpenIdConnectFilter(String defaultFilterProcessesUrl) {
        super(defaultFilterProcessesUrl);
        setAuthenticationManager(new NoopAuthenticationManager());
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest httpServletRequest,
                                                HttpServletResponse httpServletResponse)
            throws AuthenticationException {

        String code = httpServletRequest.getHeader(Defines.CODE);
        if (null != code && !code.isEmpty()) {
            OAuth2AccessToken accessToken = getAccessToken(code);
            if (null != accessToken) {
                final Map<String, String> authInfo = validateIdToken(accessToken);
                Boolean claimsVerified = verifyClaims(authInfo);
                if (claimsVerified) {
                    final OpenIdConnectUserDetails userDetails = new OpenIdConnectUserDetails(authInfo, accessToken);
                    return new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                }
            }
        }
        return null;
    }

    private OAuth2AccessToken getAccessToken(String code) {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            MultiValueMap<String, String> map = getMultiValueMap(code);
            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
            ResponseEntity<TokenModel> response = restTemplate.postForEntity(
                    restTemplate.getResource().getAccessTokenUri(), request, TokenModel.class);
            return response.getBody().getAccess_token();
            //return restTemplate.getAccessToken();
        } catch (final OAuth2Exception oAuthExp) {
            throw new BadCredentialsException(Defines.COULDNOT_OBTAIN_ACCESS_TOKEN, oAuthExp);
        }
    }

    private Map<String, String> validateIdToken(OAuth2AccessToken accessToken) {
        try {
            final String idToken = accessToken.getAdditionalInformation().get(Defines.ID_TOKEN).toString();
            String keyID = JwtHelper.headers(idToken).get(Defines.KEY_ID);
            final Jwt tokenDecoded = JwtHelper.decodeAndVerify(idToken, verifier(keyID));
            return new ObjectMapper().readValue(tokenDecoded.getClaims(), Map.class);
        } catch (final Exception exp) {
            throw new BadCredentialsException(Defines.COULDNOT_OBTAIN_TOKEN_DETAILS, exp);
        }
    }

    private MultiValueMap<String, String> getMultiValueMap(String code) {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        AuthorizationCodeResourceDetails details = (AuthorizationCodeResourceDetails) restTemplate.getResource();
        map.add(Defines.CODE, code);
        map.add(Defines.CLIENT_ID, details.getClientId());
        map.add(Defines.CLIENT_SECRET, details.getClientSecret());
        map.add(Defines.GRANT_TYPE, details.getGrantType());
        map.add(Defines.REDIRECT_URI, details.getPreEstablishedRedirectUri());
        return map;
    }

    private RsaVerifier verifier(String keyId) throws Exception {
        JwkProvider jwkProvider = new UrlJwkProvider(new URL(jwkUrl));
        Jwk jwk = jwkProvider.get(keyId);
        return new RsaVerifier((RSAPublicKey) jwk.getPublicKey());
    }

    private Boolean verifyClaims(Map<String, String> claims) {
        Object date = claims.get(Defines.EXPIRE_DATE);
        Date expireDate = new Date(Integer.parseInt(date.toString()) * Defines.EXPIRED);
        if (expireDate.before(new Date())
                || !issuer.equals(claims.get(Defines.ISSUER))
                || !clientId.equals(claims.get(Defines.AUDIENCE))) {
            throw new RuntimeException(Defines.INVALID_CLAIMS);
        }
        return true;
    }

    public OAuth2RestTemplate getRestTemplate() {
        return restTemplate;
    }

    public void setRestTemplate(OAuth2RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    private static class NoopAuthenticationManager implements AuthenticationManager {

        @Override
        public Authentication authenticate(Authentication authentication) throws AuthenticationException {
            throw new UnsupportedOperationException(Defines.NOOP_AUTHENTICATION_MANAGER);
        }

    }
}
