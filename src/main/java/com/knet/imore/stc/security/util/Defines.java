package com.knet.imore.stc.security.util;

public final class Defines {

    //public static final String ENTRY_POINT_URL = "https://localhost:8081/IMOREGATEWAY/IMORECORE/PRN/PRN/PRN/landing";
    public static final String ENTRY_POINT_URL = "/";
    public static final String RESOURCES_PATTERN = "/resources/**";

    //openid details
    public static final String CODE = "code";
    public static final String CLIENT_ID= "client_id";
    public static final String CLIENT_SECRET = "client_secret";
    public static final String GRANT_TYPE = "grant_type";
    public static final String REDIRECT_URI = "redirect_uri";

    public static final String SCOPE_OPEN_ID = "openid";
    public static final String SCOPE_EMAIL = "email";
    public static final String SCOPE_PROFILE = "profile";

    //access token details
    public static final Long EXPIRED = 1000L;
    public static final String ID_TOKEN = "id_token";
    public static final String KEY_ID = "kid";
    public static final String SUB = "sub";
    public static final String EMAIL = "email";
    public static final String ROLE_USER = "ROLE_USER";
    public static final String EXPIRE_DATE = "exp";
    public static final String ISSUER = "iss";
    public static final String AUDIENCE = "aud";

    //exception messages
    public static final String NOOP_AUTHENTICATION_MANAGER = "No authentication should be done with this AuthenticationManager";
    public static final String COULDNOT_OBTAIN_ACCESS_TOKEN = "Could not obtain access token";
    public static final String COULDNOT_OBTAIN_TOKEN_DETAILS = "Could not obtain user details from token";
    public static final String INVALID_CLAIMS = "Invalid Claims";
}
