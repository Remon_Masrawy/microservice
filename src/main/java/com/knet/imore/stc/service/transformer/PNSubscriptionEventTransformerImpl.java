package com.knet.imore.stc.service.transformer;

import com.knet.imore.stc.common.dto.SubscriptionEventDTO;
import com.knet.imore.stc.common.dto.pn.RegistrationModel;
import com.knet.imore.stc.common.dto.pn.SubscriptionModel;
import com.knet.imore.stc.service.transformation.AbstractPNDTOTransformer;
import org.springframework.stereotype.Component;

@Component("pnSubscriptionEventTransformer")
public class PNSubscriptionEventTransformerImpl
        extends AbstractPNDTOTransformer<RegistrationModel, SubscriptionEventDTO>
        implements PNSubscriptionEventTransformer {

    public PNSubscriptionEventTransformerImpl(){
        super(RegistrationModel.class, SubscriptionEventDTO.class);
    }
}
