package com.knet.imore.stc.service.transformer;

import com.knet.imore.stc.common.dto.SubscriptionEventDTO;
import com.knet.imore.stc.common.dto.pn.RegistrationModel;
import com.knet.imore.stc.common.dto.pn.SubscriptionModel;
import com.knet.imore.stc.service.transformation.PNDTOTransformer;

public interface PNSubscriptionEventTransformer extends PNDTOTransformer<RegistrationModel, SubscriptionEventDTO> {
}
