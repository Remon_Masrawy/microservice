package com.knet.imore.stc.service.management;

import java.math.BigInteger;
import java.security.SecureRandom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.stereotype.Component;


@Component("tokenGenerator")
public class TokenGenerator {

    @Value("${token.valid.period:5}")
    private Integer validPeriod;

    @Autowired
    @Qualifier("jedisConnectionFactory")
    JedisConnectionFactory connectionFactory;

    public String generateHashedToken() {
        RedisConnection connection = connectionFactory.getConnection();
        String token = generateToken();
        connection.set(token.getBytes(), token.getBytes());
        connection.expire(token.getBytes(), validPeriod);
        connection.close();
        return token;
    }

    public boolean validateToken(String hashedToken) {
        RedisConnection connection = connectionFactory.getConnection();
        byte[] value = connection.get(hashedToken.getBytes());
        boolean found = false;
        if (value != null) {
            found = true;
            connection.del(hashedToken.getBytes());
        }
        connection.close();
        return found;
    }

    private String generateToken() {
        SecureRandom random = new SecureRandom();
        return "IMoreSTC:" + new BigInteger(130, random).toString(32);
    }
}
