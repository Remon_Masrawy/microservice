package com.knet.imore.stc.service.transformer;

import com.knet.imore.stc.common.dto.CustomerEventDTO;
import com.knet.imore.stc.common.dto.pn.UserModel;
import com.knet.imore.stc.service.transformation.PNDTOTransformer;

public interface PNCustomerEventModelTransformer extends PNDTOTransformer<UserModel, CustomerEventDTO> {
}
