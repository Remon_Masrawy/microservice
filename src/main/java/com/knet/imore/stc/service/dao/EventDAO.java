package com.knet.imore.stc.service.dao;

import com.knet.imore.stc.service.model.STCEventEB;

public interface EventDAO extends AbstractDao<STCEventEB, Long> {
}
