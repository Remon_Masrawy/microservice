package com.knet.imore.stc.service.services;

import com.knet.imore.stc.common.dto.EventCallbackDTO;
import com.knet.imore.stc.common.dto.STCCallbackResponse;
import com.knet.imore.stc.common.dto.pn.BasicModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpResponse;

import java.util.Optional;

public interface HttpService {

    public Optional<BasicModel> sendRequest(String url, HttpEntity entity, HttpMethod method);

    public STCCallbackResponse sendCallback(EventCallbackDTO eventCallbackDTO, final String eventId);

    public HttpHeaders generateHeaders();
}
