package com.knet.imore.stc.service.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;

@DynamicInsert
@DynamicUpdate
@Getter
@Setter
public class ItemEB extends AbstractEntity<Long> {

    private Long id;
    private String name;
    private String description;
    private LocalDateTime created;
    private LocalDateTime modified;
    @Type(type = "json")
    private Set<UnitEB> unitEB;
    private Map<String, String> metaData;

    @Override
    public Long pk() {
        return id;
    }
}
