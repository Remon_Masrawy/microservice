package com.knet.imore.stc.service.services;

import com.knet.imore.stc.common.dto.DTO;
import com.knet.imore.stc.service.model.Entity;

import java.io.Serializable;

public interface CrudService<E extends Entity<?>, D extends DTO, PK extends Serializable> {
}
