package com.knet.imore.stc.service.transformer;

import com.knet.imore.stc.common.dto.EventDTO;
import com.knet.imore.stc.service.model.EventEB;
import com.knet.imore.stc.service.transformation.DTOTransformer;

public interface EventTransformer extends DTOTransformer<EventEB, EventDTO> {
}
