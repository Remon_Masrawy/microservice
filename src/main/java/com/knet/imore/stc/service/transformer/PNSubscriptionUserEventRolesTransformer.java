package com.knet.imore.stc.service.transformer;

import com.knet.imore.stc.common.dto.SubscriptionUserEventDTO;
import com.knet.imore.stc.common.dto.pn.RoleModel;
import com.knet.imore.stc.service.transformation.PNDTOTransformer;

public interface PNSubscriptionUserEventRolesTransformer
        extends PNDTOTransformer<RoleModel, SubscriptionUserEventDTO> {
}
