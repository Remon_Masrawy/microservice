package com.knet.imore.stc.service.model;

import com.knet.imore.stc.common.models.ExtraFieldType;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@DynamicInsert
@DynamicUpdate
@Getter
@Setter
public class ExtraFieldEB extends AbstractEntity<Long> {

    @Enumerated(EnumType.STRING)
    private ExtraFieldType type;

    @Override
    public Long pk() {
        return null;
    }
}
