package com.knet.imore.stc.service.transformer;

import com.knet.imore.stc.common.dto.EventDTO;
import com.knet.imore.stc.service.model.EventEB;
import com.knet.imore.stc.service.transformation.AbstractDTOTransformer;
import org.springframework.stereotype.Component;

@Component("eventTransformer")
public class EventTransformerImpl extends AbstractDTOTransformer<EventEB, EventDTO> implements EventTransformer {

    public EventTransformerImpl(){
        super(EventEB.class, EventDTO.class);
    }
}
