package com.knet.imore.stc.service.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.persistence.Entity;

@Entity
@DynamicInsert
@DynamicUpdate
@Getter
@Setter
@DiscriminatorValue(value = "TEST_EVENT")
public class STCTestEventEB extends STCEventEB {

    @Type(type = "json-descriptor")
    @Column(name = "BODY", length = 2000)
    private TestEventEB testEvent;

    @PrePersist
    public void prePersist(){
        this.setEventId(testEvent.pk());
    }
    @Override
    public String getEventId() {return testEvent.getId();}
}
