package com.knet.imore.stc.service.utils;

import com.knet.imore.stc.service.wrappers.GenericRequestWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.util.ContentCachingRequestWrapper;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.util.Optional;
import java.util.stream.Collectors;

public class HttpUtils {

    private static final Logger logger = LoggerFactory.getLogger(HttpUtils.class);

    public static Optional<String> getRequestBody(final GenericRequestWrapper request) {
        try{
            return Optional.ofNullable(request.getBody());
        }catch(Exception e){
            logger.trace("Could not obtain the stc request body from the http request", e);
            return Optional.ofNullable(null);
        }
    }
}
