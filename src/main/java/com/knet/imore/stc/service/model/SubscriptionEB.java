package com.knet.imore.stc.service.model;

import com.knet.imore.stc.common.models.SubscriptionCancelReason;
import com.knet.imore.stc.common.models.SubscriptionStatus;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;

@DynamicInsert
@DynamicUpdate
@Getter
@Setter
public class SubscriptionEB extends AbstractEntity<Long> {

    private Long id;
    @Type(type = "json")
    private CustomerEB customerEB;
    @Type(type = "json")
    private PriceEB priceEB;
    @Type(type = "json")
    private PlanEB planEB;
    @Enumerated(EnumType.STRING)
    private SubscriptionStatus status;
    private LocalDateTime start;
    private LocalDateTime canceledAt;
    @Enumerated(EnumType.STRING)
    private SubscriptionCancelReason cancelReason;
    private LocalDateTime created;
    private Map<String, ExtraFieldEB> extraFields;
    private Long baseSubscription;
    private Double overridePrice;
    @Type(type = "json")
    private Set<SubscriptionQuantifiableItemPriceEB> subscriptionQuantifiableItemPriceEBS;
    private Double itemsPrice;
    private LocalDateTime endDate;

    @Override
    public Long pk() {
        return id;
    }
}
