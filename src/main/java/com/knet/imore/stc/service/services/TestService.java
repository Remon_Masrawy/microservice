package com.knet.imore.stc.service.services;

import com.knet.imore.stc.common.dto.TestEventDTO;
import com.knet.imore.stc.common.dto.pn.BasicModel;
import com.knet.imore.stc.service.model.TestEventEB;

import java.util.Optional;

public interface TestService extends CrudService<TestEventEB, TestEventDTO, String> {

    public void processTestEvent(TestEventDTO testEventDTO);
    public Optional<BasicModel> expireTestEvent(TestEventDTO testEventDTO);
}
