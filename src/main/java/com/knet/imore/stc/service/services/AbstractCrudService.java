package com.knet.imore.stc.service.services;

import com.knet.imore.stc.common.dto.DTO;
import com.knet.imore.stc.service.model.Entity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.transaction.Transactional;
import java.io.Serializable;

@Transactional
public abstract class AbstractCrudService<E extends Entity<?>, D extends DTO, PK extends Serializable>
                    implements CrudService<E, D, PK>{

    public final Logger logger = LoggerFactory.getLogger(this.getClass());
}
