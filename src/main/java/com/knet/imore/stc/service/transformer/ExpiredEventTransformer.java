package com.knet.imore.stc.service.transformer;

import com.knet.imore.stc.common.dto.ExpiredEventDTO;
import com.knet.imore.stc.service.model.ExpiredEventEB;
import com.knet.imore.stc.service.transformation.DTOTransformer;

public interface ExpiredEventTransformer extends DTOTransformer<ExpiredEventEB, ExpiredEventDTO> {
}
