package com.knet.imore.stc.service.dao;

import com.knet.imore.stc.service.model.STCCustomerEventEB;

public interface CustomerEventDAO extends AbstractDao<STCCustomerEventEB, Long> {
}
