package com.knet.imore.stc.service.dao;

import com.knet.imore.stc.service.model.STCExpiredEventEB;

public interface ExpiredEventDAO extends AbstractDao<STCExpiredEventEB, Long> {
}
