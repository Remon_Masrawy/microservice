package com.knet.imore.stc.service.management;

import java.time.LocalDateTime;
import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component("systemInitializer")
@Order(value = Ordered.HIGHEST_PRECEDENCE)
public class SystemInitializer {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @PostConstruct
    public void init() {
        // Set system timezone to default GMT
        System.setProperty("user.timezone", "UTC");
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        log.info("*************SystemInitializer.init( Initializing System parameters @)" + LocalDateTime.now());
    }

}
