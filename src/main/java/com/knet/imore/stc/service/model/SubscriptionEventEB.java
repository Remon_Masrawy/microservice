package com.knet.imore.stc.service.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import javax.persistence.*;

@Getter
@Setter
@DynamicInsert
@DynamicUpdate
public class SubscriptionEventEB extends EventEB implements Entity<String> {

    @Type(type = "json")
    private SubscriptionEB subscriptionEB;
}
