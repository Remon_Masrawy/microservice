package com.knet.imore.stc.service.services;

import com.knet.imore.stc.common.dto.CustomerEventDTO;
import com.knet.imore.stc.common.dto.pn.BasicModel;
import com.knet.imore.stc.service.model.CustomerEventEB;

import java.util.Optional;

public interface CustomerService extends CrudService<CustomerEventEB, CustomerEventDTO, String> {

    public void processCustomer(CustomerEventDTO customerEventDTO);
    public Optional<BasicModel> expireCustomer(CustomerEventDTO customerEventDTO);
}
