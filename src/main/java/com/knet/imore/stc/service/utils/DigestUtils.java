package com.knet.imore.stc.service.utils;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.HmacAlgorithms;
import org.apache.commons.codec.digest.HmacUtils;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.DigestException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DigestUtils {

    public static Optional<String> hmacSHA256Base64(String key, String code) {
        return Optional.of(Base64.getEncoder().encodeToString(new HmacUtils(HmacAlgorithms.HMAC_SHA_256, key).hmac(code)));
    }

    public static Optional<String> hmacSHA256Base64(byte[] key, byte[] code) {
        return Optional.of(Base64.getEncoder().encodeToString(new HmacUtils(HmacAlgorithms.HMAC_SHA_256, key).hmac(code)));
    }

    protected Optional<byte[]> sign(final String stringData, final byte[] key) {
        return Optional.of(new HmacUtils(HmacAlgorithms.HMAC_SHA_256, key).hmac(stringData));
    }

    public static final String generateUUID() throws DigestException{
        try {
            MessageDigest salt = MessageDigest.getInstance("SHA-512");
            salt.update(UUID.randomUUID().toString().getBytes(StandardCharsets.UTF_8));
            String digest = Hex.encodeHexString(salt.digest());
            return digest;
        }catch(NoSuchAlgorithmException ex){
            throw new DigestException("couldn't make digest of partial content");
        }
    }

}
