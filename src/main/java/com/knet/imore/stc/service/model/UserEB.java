package com.knet.imore.stc.service.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

@DynamicInsert
@DynamicUpdate
@Getter
@Setter
public class UserEB extends AbstractEntity<Long>  {

    private Long id;
    private String username;
    private String firstName;
    private String lastName;
    private String email;
    @Override
    public Long pk() {
        return id;
    }
}
