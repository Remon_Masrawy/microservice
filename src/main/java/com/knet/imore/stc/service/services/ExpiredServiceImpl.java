package com.knet.imore.stc.service.services;

import com.knet.imore.stc.common.dto.*;
import com.knet.imore.stc.common.dto.pn.BasicModel;
import com.knet.imore.stc.common.models.EventStatus;
import com.knet.imore.stc.service.dao.ExpiredEventDAO;
import com.knet.imore.stc.service.model.ExpiredEventEB;
import com.knet.imore.stc.service.model.STCExpiredEventEB;
import com.knet.imore.stc.service.transformer.ExpiredEventTransformer;
import com.knet.imore.stc.service.utils.HelperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

import static com.knet.imore.stc.common.models.ErrorConstant.INTERNAL_SERVER_ERROR;

@Transactional
@Service("expiredService")
public class ExpiredServiceImpl extends AbstractCrudService<ExpiredEventEB, ExpiredEventDTO, String>
        implements ExpiredService {

    @Autowired
    private ExpiredEventDAO expiredEventDAO;
    @Autowired
    private TestService testService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private SubscriptionService subscriptionService;
    @Autowired
    private EventCallbackService eventCallbackService;
    @Autowired
    private SubscriptionUserService subscriptionUserService;
    @Autowired
    private ExpiredEventTransformer expiredEventTransformer;

    @Override
    @Async("threadPoolTaskExecutor")
    public void expireEvent(ExpiredEventDTO expiredEventDTO){
        STCExpiredEventEB stcExpiredEventEB = new STCExpiredEventEB();
        try{
            stcExpiredEventEB.setExpiredEvent(expiredEventTransformer.transformDTOToEntity(expiredEventDTO));
            expiredEventDAO.save(stcExpiredEventEB);
            Optional<BasicModel> basicModel = expire(expiredEventDTO);
            eventCallbackService.handleCallback(basicModel.get(), stcExpiredEventEB);
        }catch(Exception e){
            logger.error("Error: expire event with id {} and type {} aborted due to",
                    expiredEventDTO.getId(), expiredEventDTO.getType().getValue(), e);
            eventCallbackService.generateCallback(stcExpiredEventEB, EventStatus.ERROR, INTERNAL_SERVER_ERROR);
        }
    }

    private Optional<BasicModel> expire(ExpiredEventDTO expiredEventDTO){
        logger.debug("Begin expire event:{} with type:{}",
                expiredEventDTO.getEventDTO().getId(), expiredEventDTO.getEventDTO().getType());
        Optional<BasicModel> basicModel = Optional.empty();
        switch(HelperUtils.findEventCategory(expiredEventDTO.getEventDTO().getType())){
            case SUBSCRIPTION_EVENT:
                basicModel = subscriptionService.expireSubscription((SubscriptionEventDTO) expiredEventDTO.getEventDTO());
                break;
            case CUSTOMER_EVENT:
                basicModel = customerService.expireCustomer((CustomerEventDTO) expiredEventDTO.getEventDTO());
                break;
            case SUBSCRIPTION_USER_EVENT:
                basicModel = subscriptionUserService.expireSubscriptionUser((SubscriptionUserEventDTO) expiredEventDTO.getEventDTO());
                break;
            case TEST_EVENT:
                basicModel = testService.expireTestEvent((TestEventDTO) expiredEventDTO.getEventDTO());
                break;
        }
        logger.debug("End expire event:{}", expiredEventDTO.getEventDTO().getId());
        return basicModel;
    }
}
