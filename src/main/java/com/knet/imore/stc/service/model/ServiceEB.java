package com.knet.imore.stc.service.model;

import com.knet.imore.stc.common.models.ServiceStatus;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;

@DynamicInsert
@DynamicUpdate
@Getter
@Setter
public class ServiceEB extends AbstractEntity<Long> {

    private Long id;
    @Enumerated(EnumType.STRING)
    private ServiceStatus status;
    private String name;
    private String description;
    private String url;
    @Type(type = "json")
    private Set<ItemEB> items;
    @Type(type = "json")
    private Set<FeatureEB> features;
    @Type(type = "json")
    private Set<PlanEB> plans;
    // Set of bundles
    private Map<String, String> metaData;
    private LocalDateTime created;
    private LocalDateTime modified;
    @Override
    public Long pk() {
        return id;
    }
}
