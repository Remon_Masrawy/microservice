package com.knet.imore.stc.service.transformer;

import com.knet.imore.stc.common.dto.SubscriptionEventDTO;
import com.knet.imore.stc.common.dto.SubscriptionUserEventDTO;
import com.knet.imore.stc.service.model.SubscriptionEventEB;
import com.knet.imore.stc.service.model.SubscriptionUserEventEB;
import com.knet.imore.stc.service.transformation.DTOTransformer;

public interface SubscriptionUserEventTransformer
        extends DTOTransformer<SubscriptionUserEventEB, SubscriptionUserEventDTO> {
}
