package com.knet.imore.stc.service.transformer;

import com.knet.imore.stc.common.dto.CustomerEventDTO;
import com.knet.imore.stc.service.model.CustomerEventEB;
import com.knet.imore.stc.service.transformation.DTOTransformer;

public interface CustomerEventTransformer extends DTOTransformer<CustomerEventEB, CustomerEventDTO> {
}
