package com.knet.imore.stc.service.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

@DynamicInsert
@DynamicUpdate
@Getter
@Setter
public class UnitEB extends AbstractEntity<String> {

    private String id;
    private String name;
    private String shortName;
    private String symbol;

    @Override
    public String pk() {
        return id;
    }
}
