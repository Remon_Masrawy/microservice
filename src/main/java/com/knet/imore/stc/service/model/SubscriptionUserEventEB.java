package com.knet.imore.stc.service.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.persistence.Entity;

@Getter
@Setter
@DynamicInsert
@DynamicUpdate
public class SubscriptionUserEventEB extends EventEB {

    @Type(type = "json")
    private SubscriptionUserEB subscriptionUserEB;
}
