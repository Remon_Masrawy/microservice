package com.knet.imore.stc.service.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import java.util.Map;

@DynamicInsert
@DynamicUpdate
@Getter
@Setter
public class QuantifiableItemEB extends AbstractEntity<Long> {

    private String name;
    private String description;
    @Type(type = "json")
    private UnitEB unit;
    private Map<String, String> metaData;

    @Override
    public Long pk() {
        return null;
    }
}
