package com.knet.imore.stc.service.services;

import com.knet.imore.stc.common.dto.SubscriptionUserEventDTO;
import com.knet.imore.stc.common.dto.pn.BasicModel;
import com.knet.imore.stc.common.dto.pn.RegistrationModel;
import com.knet.imore.stc.common.dto.pn.RoleModel;
import com.knet.imore.stc.common.dto.pn.UserModel;
import com.knet.imore.stc.common.exceptions.UserSubscriptionAdditionException;
import com.knet.imore.stc.common.exceptions.UserSubscriptionRemoveException;
import com.knet.imore.stc.common.exceptions.UserSubscriptionUpdateRolesException;
import com.knet.imore.stc.common.models.EventStatus;
import com.knet.imore.stc.common.models.Gender;
import com.knet.imore.stc.common.models.UserType;
import com.knet.imore.stc.service.dao.SubscriptionUserEventDAO;
import com.knet.imore.stc.service.management.TokenGenerator;
import com.knet.imore.stc.service.model.STCSubscriptionUserEventEB;
import com.knet.imore.stc.service.model.SubscriptionUserEventEB;
import com.knet.imore.stc.service.transformer.PNSubscriptionUserEventRolesTransformer;
import com.knet.imore.stc.service.transformer.PNSubscriptionUserEventTransformer;
import com.knet.imore.stc.service.transformer.SubscriptionUserEventTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

import static com.knet.imore.stc.common.models.ErrorConstant.SUCCESS;
import static com.knet.imore.stc.common.models.ErrorConstant.INTERNAL_SERVER_ERROR;

@Transactional
@Service("subscriptionUserService")
public class SubscriptionUserServiceImpl
        extends AbstractCrudService<SubscriptionUserEventEB, SubscriptionUserEventDTO, String>
        implements SubscriptionUserService {

    @Autowired
    private HttpService httpService;

    @Autowired
    private EventCallbackService eventCallbackService;

    @Autowired
    private SubscriptionUserEventDAO subscriptionUserEventDAO;

    @Autowired
    private SubscriptionUserEventTransformer subscriptionUserEventTransformer;

    @Autowired
    private PNSubscriptionUserEventTransformer pnSubscriptionUserEventTransformer;

    @Autowired
    private PNSubscriptionUserEventRolesTransformer pnSubscriptionUserEventRolesTransformer;


    @Override
    @Async("threadPoolTaskExecutor")
    public void processSubscriptionUser(SubscriptionUserEventDTO subscriptionUserEventDTO){
        STCSubscriptionUserEventEB stcSubscriptionUserEventEB = new STCSubscriptionUserEventEB();
        try {
            stcSubscriptionUserEventEB.setSubscriptionUserEvent(subscriptionUserEventTransformer.
                    transformDTOToEntity(subscriptionUserEventDTO));
            subscriptionUserEventDAO.save(stcSubscriptionUserEventEB);
            Optional<BasicModel> basicModel = Optional.empty();
            switch (subscriptionUserEventDTO.getType()) {
                case SUBSCRIPTION_USER_ADDED:
                    basicModel = addUserToSubscription(subscriptionUserEventDTO);
                    break;
                case SUBSCRIPTION_USER_REMOVED:
                    basicModel = removeUserFromSubscription(subscriptionUserEventDTO);
                    break;
                case SUBSCRIPTION_USER_ROLES_CHANGED:
                    basicModel = changeUserRoles(subscriptionUserEventDTO);
                    break;
            }
            eventCallbackService.handleCallback(basicModel.get(), stcSubscriptionUserEventEB);
        }catch(UserSubscriptionAdditionException
                | UserSubscriptionRemoveException
                | UserSubscriptionUpdateRolesException e){
            eventCallbackService.generateCallback(stcSubscriptionUserEventEB, EventStatus.ERROR, INTERNAL_SERVER_ERROR);
        }catch(Exception e){
            logger.error("Error: process subscription user for event with id {} and type {} aborted due to",
                    subscriptionUserEventDTO.getType().getValue(), subscriptionUserEventDTO.getId(), e);
            eventCallbackService.generateCallback(stcSubscriptionUserEventEB, EventStatus.ERROR, INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    @Async("threadPoolTaskExecutor")
    public Optional<BasicModel> expireSubscriptionUser(SubscriptionUserEventDTO subscriptionUserEventDTO){
        return Optional.empty();
    }

    private Optional<BasicModel> addUserToSubscription(SubscriptionUserEventDTO subscriptionUserEventDTO){
        RegistrationModel registrationModel = constructRegistrationModel(subscriptionUserEventDTO);
        HttpEntity<RegistrationModel> entity = new HttpEntity<>(registrationModel, httpService.generateHeaders());
        String url = "PRN/PRN/PRN/onlineRegistration/assignUsers";
        try {
            Optional<BasicModel> basicModel = httpService.sendRequest(url, entity, HttpMethod.POST);
            return basicModel;
        }catch(Exception e){
            logger.error("Error: exception thrown when trying to call property net micro service to add user to subscription", e);
            throw new UserSubscriptionAdditionException();
        }
    }


    private Optional<BasicModel> removeUserFromSubscription(SubscriptionUserEventDTO subscriptionUserEventDTO){
        HttpEntity<Void> entity = new HttpEntity<>(httpService.generateHeaders());
        final String url = "PRN/PRN/PRN/onlineRegistration/deleteUsers/{providerId}"
                .replace("{providerId}", String.valueOf(subscriptionUserEventDTO.getSubscriptionUserDTO().getId()));
        try {
            Optional<BasicModel> basicModel = httpService.sendRequest(url, entity, HttpMethod.DELETE);
            return basicModel;
        }catch(Exception e){
            logger.error("Error: exception thrown when trying to call property net micro service to remove user from subscription", e);
            throw new UserSubscriptionRemoveException();
        }
    }

    private Optional<BasicModel> changeUserRoles(SubscriptionUserEventDTO subscriptionUserEventDTO){
        RoleModel roleModel = pnSubscriptionUserEventRolesTransformer.transformDTOToModel(subscriptionUserEventDTO);
        HttpEntity<RoleModel> entity = new HttpEntity<>(roleModel, httpService.generateHeaders());
        final String url = "PRN/PRN/PRN/onlineRegistration/assignRole/{providerId}"
                .replace("{providerId}", String.valueOf(subscriptionUserEventDTO.getSubscriptionUserDTO().getId()));
        try {
            Optional<BasicModel> basicModel = httpService.sendRequest(url, entity, HttpMethod.PUT);
            return basicModel;
        }catch(Exception e){
            logger.error("Error: exception thrown when trying to call property net micro service to change user roles", e);
            throw new UserSubscriptionUpdateRolesException();
        }
    }

    private RegistrationModel constructRegistrationModel(SubscriptionUserEventDTO subscriptionUserEventDTO) {
        RegistrationModel registrationModel = pnSubscriptionUserEventTransformer.transformDTOToModel(subscriptionUserEventDTO);
        registrationModel.getEndUser().setType(UserType.NORMAL_USER);
        registrationModel.getEndUser().setGender(Gender.MALE);
        return registrationModel;
    }
}
