package com.knet.imore.stc.service.model;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

@Getter
@Setter
@DynamicInsert
@DynamicUpdate
public class CustomerEventEB extends EventEB {

    @Type(type = "json")
    private CustomerEB customerEB;
}
