package com.knet.imore.stc.service.transformer;

import com.knet.imore.stc.common.dto.TestEventDTO;
import com.knet.imore.stc.service.model.TestEventEB;
import com.knet.imore.stc.service.transformation.AbstractDTOTransformer;
import org.springframework.stereotype.Component;

@Component("testEventTransformer")
public class TestEventTransformerImpl
        extends AbstractDTOTransformer<TestEventEB, TestEventDTO> implements TestEventTransformer {

    public TestEventTransformerImpl(){
        super(TestEventEB.class, TestEventDTO.class);
    }
}
