package com.knet.imore.stc.service.model;

import java.time.LocalDateTime;
import javax.persistence.*;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import lombok.Setter;
import lombok.Getter;

import com.knet.imore.stc.common.models.EventType;

@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@MappedSuperclass
public abstract class EventEB extends AbstractEntity<String> {

    @Id
    private String id;

    @Enumerated(EnumType.STRING)
    private EventType type;

    @Type(type = "json")
    private ServiceEB serviceEB;

    private LocalDateTime createdAt;

    private String apiVersion;

    @Override
    public String pk() {
        return id;
    }
}
