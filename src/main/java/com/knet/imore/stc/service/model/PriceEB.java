package com.knet.imore.stc.service.model;

import com.knet.imore.stc.common.models.PeriodUnit;
import com.knet.imore.stc.common.models.PriceType;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;

@DynamicInsert
@DynamicUpdate
@Getter
@Setter
public class PriceEB extends AbstractEntity<Long> {

    private Long id;

    @Enumerated(EnumType.STRING)
    private PriceType type;
    private Integer period;
    @Enumerated(EnumType.STRING)
    private PeriodUnit periodUnit;

    private Double amount;
    private String currency;
    private LocalDateTime created;
    private LocalDateTime modified;

    @Override
    public Long pk() {
        return id;
    }
}
