package com.knet.imore.stc.service.transformation;

import lombok.extern.slf4j.Slf4j;
import org.dozer.CustomConverter;
import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Slf4j
@Component("dozerMapper")
public class DozerMapperImpl extends DozerBeanMapper implements DozerMapper {

    @Autowired
    private ApplicationContext context;

    @Value(value = "classpath:dozer-mapping/*.xml")
    private Resource[] resources;

    @PostConstruct
    public void init() throws IOException{
        log.info("Begin loading dozer mapping .xml files under 'dozer-mapping' resource and custom converters.");
        loadFiles();
        loadConvertersWithId();
    }

    private void loadFiles() throws IOException{
        List<String> mappingFileUrlList = new ArrayList<>();
        for (Resource resource : resources) {
            mappingFileUrlList.add(String.valueOf(resource.getURL()));
        }
        mappingFileUrlList.addAll(Collections.singletonList("dozerJdk8Converters.xml"));
        setMappingFiles(mappingFileUrlList);
    }

    private void loadConvertersWithId() {
        Map<String, CustomConverter> converters = context.getBeansOfType(CustomConverter.class);
        setCustomConvertersWithId(converters);
    }

}
