package com.knet.imore.stc.service.services;

import com.knet.imore.stc.common.dto.ExpiredEventDTO;
import com.knet.imore.stc.service.model.ExpiredEventEB;

public interface ExpiredService extends CrudService<ExpiredEventEB, ExpiredEventDTO, String> {

    public void expireEvent(ExpiredEventDTO expiredEventDTO);
}
