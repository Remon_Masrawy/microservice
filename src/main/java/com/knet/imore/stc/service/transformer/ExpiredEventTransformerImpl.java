package com.knet.imore.stc.service.transformer;

import com.knet.imore.stc.common.dto.ExpiredEventDTO;
import com.knet.imore.stc.service.model.ExpiredEventEB;
import com.knet.imore.stc.service.transformation.AbstractDTOTransformer;
import org.springframework.stereotype.Component;

@Component("expiredEventTransformer")
public class ExpiredEventTransformerImpl extends AbstractDTOTransformer<ExpiredEventEB, ExpiredEventDTO>
        implements ExpiredEventTransformer {

    public ExpiredEventTransformerImpl(){
        super(ExpiredEventEB.class, ExpiredEventDTO.class);
    }
}
