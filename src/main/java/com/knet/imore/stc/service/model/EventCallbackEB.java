package com.knet.imore.stc.service.model;

import com.knet.imore.stc.common.dto.STCCallbackResponse;
import com.knet.imore.stc.common.models.EventStatus;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Setter
@Getter
@DynamicInsert
@DynamicUpdate
public class EventCallbackEB extends AbstractEntity<Long> {

    @Enumerated(EnumType.STRING)
    private EventStatus status;

    private String message;

    private Long refNumber;

    private String landingPageURL;

    private String managementPageURL;

    private String instructions;

    private STCCallbackResponse response;

    @Override
    public Long pk() {
        return refNumber;
    }
}
