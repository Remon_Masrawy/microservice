package com.knet.imore.stc.service.services;

import com.knet.imore.stc.common.dto.SubscriptionUserEventDTO;
import com.knet.imore.stc.common.dto.pn.BasicModel;
import com.knet.imore.stc.service.model.SubscriptionUserEventEB;

import java.util.Optional;

public interface SubscriptionUserService extends CrudService<SubscriptionUserEventEB, SubscriptionUserEventDTO, String> {

    public void processSubscriptionUser(SubscriptionUserEventDTO subscriptionUserEventDTO);
    public Optional<BasicModel> expireSubscriptionUser(SubscriptionUserEventDTO subscriptionUserEventDTO);
}
