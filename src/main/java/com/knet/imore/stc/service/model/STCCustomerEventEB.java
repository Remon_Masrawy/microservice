package com.knet.imore.stc.service.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.PrePersist;

@Entity
@DynamicInsert
@DynamicUpdate
@Getter
@Setter
@DiscriminatorValue(value = "CUSTOMER_EVENT")
public class STCCustomerEventEB extends STCEventEB {

    @Type(type = "json")
    @Column(name = "BODY", length = 2000)
    private CustomerEventEB customerEvent;

    @PrePersist
    public void prePersist(){
        this.setEventId(customerEvent.pk());
    }
    @Override
    public String getEventId() {return customerEvent.getId();}
}
