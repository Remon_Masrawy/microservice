package com.knet.imore.stc.service.audit;

import java.util.Optional;

import org.springframework.data.domain.AuditorAware;


public class EntityAuditAware implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {
        return Optional.empty();
    }
}
