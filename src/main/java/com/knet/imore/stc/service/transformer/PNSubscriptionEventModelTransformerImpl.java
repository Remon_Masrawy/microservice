package com.knet.imore.stc.service.transformer;

import com.knet.imore.stc.common.dto.SubscriptionEventDTO;
import com.knet.imore.stc.common.dto.pn.SubscriptionModel;
import com.knet.imore.stc.service.transformation.AbstractPNDTOTransformer;
import org.springframework.stereotype.Component;

@Component("pnSubscriptionEventModelTransformer")
public class PNSubscriptionEventModelTransformerImpl
        extends AbstractPNDTOTransformer<SubscriptionModel, SubscriptionEventDTO>
        implements PNSubscriptionEventModelTransformer {

    public PNSubscriptionEventModelTransformerImpl(){
        super(SubscriptionModel.class, SubscriptionEventDTO.class);
    }
}
