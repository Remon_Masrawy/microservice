package com.knet.imore.stc.service.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

@DynamicInsert
@DynamicUpdate
@Getter
@Setter
public class PlanItemEB extends AbstractEntity<Long>  {

    private Long id;
    @Type(type = "json")
    private ItemEB item;
    @Type(type = "json")
    private UnitEB unit;
    private Double quantity;

    @Override
    public Long pk() {
        return id;
    }
}
