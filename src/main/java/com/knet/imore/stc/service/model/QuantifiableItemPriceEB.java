package com.knet.imore.stc.service.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

@DynamicInsert
@DynamicUpdate
@Getter
@Setter
public class QuantifiableItemPriceEB extends AbstractEntity<Long> {

    @Type(type = "json")
    private QuantifiableItemEB quantifiableItem;
    private String currency;
    private Double price;
    private Integer min;
    private Integer max;
    @Override
    public Long pk() {
        return null;
    }
}
