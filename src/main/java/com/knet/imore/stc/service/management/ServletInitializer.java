package com.knet.imore.stc.service.management;

import com.knet.imore.stc.web.boot.IMoreSTCBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        IMoreSTCBootApplication.addManagementSecurityCredentials();
        IMoreSTCBootApplication.addCustomPlatformProperties();
        IMoreSTCBootApplication.trustAllCertificates();
        return application.sources(IMoreSTCBootApplication.class);
    }
}
