package com.knet.imore.stc.service.transformer;

import com.knet.imore.stc.common.dto.SubscriptionUserEventDTO;
import com.knet.imore.stc.common.dto.pn.RegistrationModel;
import com.knet.imore.stc.service.transformation.DTOTransformer;
import com.knet.imore.stc.service.transformation.PNDTOTransformer;

public interface PNSubscriptionUserEventTransformer
        extends PNDTOTransformer<RegistrationModel, SubscriptionUserEventDTO> {
}
