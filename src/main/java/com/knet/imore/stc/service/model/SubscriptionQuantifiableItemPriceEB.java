package com.knet.imore.stc.service.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

@DynamicInsert
@DynamicUpdate
@Getter
@Setter
public class SubscriptionQuantifiableItemPriceEB extends AbstractEntity<Long>{

    @Type(type = "json")
    private QuantifiableItemPriceEB quantifiableItemPrice;
    private Integer quantity;

    @Override
    public Long pk() {
        return null;
    }
}
