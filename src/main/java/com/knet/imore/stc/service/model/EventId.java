package com.knet.imore.stc.service.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Objects;

@Getter
@Setter
public class EventId implements Serializable {
    private Long id;
    private String eventId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EventId eventId1 = (EventId) o;
        return eventId.equals(eventId1.eventId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(eventId);
    }
}
