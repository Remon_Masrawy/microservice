package com.knet.imore.stc.service.configuration;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.knet.imore.stc.common.models.*;
import com.knet.imore.stc.service.filters.RequestSignatureFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Configuration
public class WebConfiguration extends WebMvcConfigurationSupport {

    @Autowired
    private Environment env;

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(String.class, LocalDateTime.class,
                timestamp -> LocalDateTime.parse(timestamp, DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        registry.addConverter(EventStatus.class, String.class, eventStatus -> eventStatus.getValue());
        registry.addConverter(String.class, PlanType.class, PlanType::convert);
        registry.addConverter(String.class, EventType.class, EventType::convert);
        registry.addConverter(String.class, PriceType.class, PriceType::convert);
        registry.addConverter(String.class, PeriodUnit.class, PeriodUnit::convert);
        registry.addConverter(String.class, ServiceStatus.class, ServiceStatus::convert);
        registry.addConverter(String.class, ExtraFieldType.class, ExtraFieldType::convert);
        registry.addConverter(String.class, SubscriptionStatus.class, SubscriptionStatus::convert);
        registry.addConverter(String.class, SubscriptionCancelReason.class, SubscriptionCancelReason::convert);
    }

    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(mappingJackson2HttpMessageConverter());
        super.addDefaultHttpMessageConverters(converters);
    }

    @Bean
    public ObjectMapper objectMapper(){
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.READ_ENUMS_USING_TO_STRING, true);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true);
        return objectMapper;
    }

    @Bean
    public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter() {
        MappingJackson2HttpMessageConverter jsonConverter = new MappingJackson2HttpMessageConverter();
        jsonConverter.setObjectMapper(objectMapper());
        return jsonConverter;
    }

    @Bean
    public RequestSignatureFilter requestSignatureFilter(){
        RequestSignatureFilter requestSignatureFilter = new RequestSignatureFilter();
        Set<String> excludedPaths =
                Stream.of(
                        env.getProperty("management.server.servlet.context-path"),
                        env.getProperty("exclude.oauth2.authenticate.stc.users")
                ).collect(Collectors.toSet());
        requestSignatureFilter.setExcludedPaths(excludedPaths);
        return requestSignatureFilter;
    }

    @Bean
    @LoadBalanced
    @Primary
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }


}
