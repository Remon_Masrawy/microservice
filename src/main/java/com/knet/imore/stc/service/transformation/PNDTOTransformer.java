package com.knet.imore.stc.service.transformation;

import com.knet.imore.stc.common.dto.DTO;
import com.knet.imore.stc.common.dto.pn.BasicModel;

import java.util.Set;

public interface PNDTOTransformer<T extends BasicModel, D extends DTO> {

    // ********************** BasicModel TO DTO transformation methods ****************************
    public D transformModelToDTO(T sourceModel);

    public void transformModelToDTO(T sourceModel, D destinationDTO);

    public Set<D> transformModelToDTO(Set<T> models);

    // ********************** DTO TO Entity transformation methods ****************************
    public void transformDTOToModel(D sourceDTO, T destinationModel);

    public T transformDTOToModel(D sourceDTO);

    public Set<T> transformDTOToModel(Set<D> dtos);
}
