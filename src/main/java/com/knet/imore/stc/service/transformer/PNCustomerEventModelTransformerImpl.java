package com.knet.imore.stc.service.transformer;

import com.knet.imore.stc.common.dto.CustomerEventDTO;
import com.knet.imore.stc.common.dto.pn.UserModel;
import com.knet.imore.stc.service.transformation.AbstractPNDTOTransformer;
import org.springframework.stereotype.Component;

@Component("pnCustomerEventModelTransformer")
public class PNCustomerEventModelTransformerImpl extends AbstractPNDTOTransformer<UserModel, CustomerEventDTO>
        implements PNCustomerEventModelTransformer {

    public PNCustomerEventModelTransformerImpl(){
        super(UserModel.class, CustomerEventDTO.class);
    }
}
