package com.knet.imore.stc.service.services;

import com.knet.imore.stc.common.dto.CustomerEventDTO;
import com.knet.imore.stc.common.dto.pn.BasicModel;
import com.knet.imore.stc.common.dto.pn.RegistrationModel;
import com.knet.imore.stc.common.dto.pn.UserModel;
import com.knet.imore.stc.common.exceptions.CustomerUpdateException;
import com.knet.imore.stc.common.exceptions.UserSubscriptionAdditionException;
import com.knet.imore.stc.common.models.EventStatus;
import com.knet.imore.stc.service.dao.CustomerEventDAO;
import com.knet.imore.stc.service.model.CustomerEventEB;
import com.knet.imore.stc.service.model.STCCustomerEventEB;
import com.knet.imore.stc.service.transformer.CustomerEventTransformer;
import com.knet.imore.stc.service.transformer.PNCustomerEventModelTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

import static com.knet.imore.stc.common.models.ErrorConstant.INTERNAL_SERVER_ERROR;

@Transactional
@Service("customerService")
public class CustomerServiceImpl extends AbstractCrudService<CustomerEventEB, CustomerEventDTO, String>
        implements CustomerService {

    @Autowired
    private HttpService httpService;

    @Autowired
    private CustomerEventDAO customerEventDAO;

    @Autowired
    private EventCallbackService eventCallbackService;

    @Autowired
    private CustomerEventTransformer customerEventTransformer;

    @Autowired
    private PNCustomerEventModelTransformer pnCustomerEventModelTransformer;

    @Override
    @Async("threadPoolTaskExecutor")
    public void processCustomer(CustomerEventDTO customerEventDTO) {
        STCCustomerEventEB stcCustomerEventEB = new STCCustomerEventEB();
        try {
            stcCustomerEventEB.setCustomerEvent(customerEventTransformer.transformDTOToEntity(customerEventDTO));
            customerEventDAO.save(stcCustomerEventEB);
            Optional<BasicModel> basicModel = Optional.empty();
            switch (customerEventDTO.getType()) {
                case ACCOUNT_RESUMED:
                case ACCOUNT_SUSPENDED:
                case ACCOUNT_TERMINATED:
                case ACCOUNT_BILLING_ACTIVATED:
                case ACCOUNT_TRIAL_FIRST_SUSPENDED:
                case ACCOUNT_TRIAL_SECOND_SUSPENDED:
                case ACCOUNT_TRIAL_FINAL_SUSPENDED:
                    basicModel = updateCustomer(customerEventDTO);
                    break;
            }
            eventCallbackService.handleCallback(basicModel.get(), stcCustomerEventEB);
        }catch(CustomerUpdateException e){
            eventCallbackService.generateCallback(stcCustomerEventEB, EventStatus.ERROR, INTERNAL_SERVER_ERROR);
        }catch(Exception e){
            logger.error("Error: process customer for event with id {} and type {} aborted due to",
                    customerEventDTO.getType().getValue(), customerEventDTO.getId(), e);
            eventCallbackService.generateCallback(stcCustomerEventEB, EventStatus.ERROR, INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    @Async("threadPoolTaskExecutor")
    public Optional<BasicModel> expireCustomer(CustomerEventDTO customerEventDTO){
        return Optional.empty();
    }

    private Optional<BasicModel> updateCustomer(CustomerEventDTO customerEventDTO){
        UserModel userModel = pnCustomerEventModelTransformer.transformDTOToModel(customerEventDTO);
        HttpEntity<UserModel> entity = new HttpEntity<>(userModel, httpService.generateHeaders());
        String url = "PRN/PRN/PRN/onlineRegistration/updateUserStatus";
        try {
            Optional<BasicModel> basicModel = httpService.sendRequest(url, entity, HttpMethod.PUT);
            return basicModel;
        }catch(Exception e){
            logger.error("Error: exception thrown when trying to call property net micro service to update customer", e);
            throw new CustomerUpdateException();
        }
    }
}
