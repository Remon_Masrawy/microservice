package com.knet.imore.stc.service.filters;

import java.util.Set;
import java.util.Optional;
import java.io.IOException;
import java.security.MessageDigest;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.filter.OncePerRequestFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.commons.codec.binary.StringUtils;

import com.knet.imore.stc.common.exceptions.EmptySecretKeyException;
import com.knet.imore.stc.common.exceptions.EventEmptyBodyException;
import com.knet.imore.stc.common.exceptions.RequestParamNotFoundException;
import com.knet.imore.stc.common.exceptions.UnknownEventEngineException;

import com.knet.imore.stc.service.utils.DigestUtils;
import com.knet.imore.stc.service.utils.HttpUtils;
import com.knet.imore.stc.service.wrappers.GenericRequestWrapper;


public class RequestSignatureFilter extends OncePerRequestFilter {

    @Value("${stc.imore-service.secret-key}")
    private String secretKey;

    private static final String X_CARTWHEEL_SIGNATURE_HEADER = "X-Cartwheel-Signature";

    private Set<String> excludedPaths;

    private static final Logger logger = LoggerFactory.getLogger(RequestSignatureFilter.class);

    @Override
    protected void doFilterInternal(
            HttpServletRequest httpServletRequest,
            HttpServletResponse httpServletResponse,
            FilterChain filterChain) throws ServletException, IOException {

        GenericRequestWrapper requestWrapper = new GenericRequestWrapper(httpServletRequest);
        String headerParam = Optional.ofNullable(httpServletRequest.getHeader(X_CARTWHEEL_SIGNATURE_HEADER))
                .orElseThrow(RequestParamNotFoundException::new);
        String key = Optional.ofNullable(secretKey).orElseThrow(EmptySecretKeyException::new);
        String body =  HttpUtils.getRequestBody(requestWrapper).orElseThrow(EventEmptyBodyException::new);
        Optional<String> signature = DigestUtils.hmacSHA256Base64(key, body);
        boolean valid = MessageDigest.isEqual(StringUtils.getBytesUtf8(headerParam),
                StringUtils.getBytesUtf8(signature.get()));
        if(!valid){
            throw new UnknownEventEngineException();
        }
        filterChain.doFilter(requestWrapper, httpServletResponse);
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
        if(request.getMethod().equals("GET")){
            return true;
        }
        if(excludedPaths != null){
              return excludedPaths.stream().anyMatch(path -> request.getRequestURI().contains(path));
        }
        return super.shouldNotFilter(request);
    }

    public void setExcludedPaths(Set<String> excludedPaths) {
        this.excludedPaths = excludedPaths;
    }
}
