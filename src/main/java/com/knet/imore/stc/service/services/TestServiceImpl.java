package com.knet.imore.stc.service.services;

import com.knet.imore.stc.common.dto.TestEventDTO;
import com.knet.imore.stc.common.dto.pn.BasicModel;
import com.knet.imore.stc.common.models.EventStatus;
import com.knet.imore.stc.service.dao.TestEventDAO;
import com.knet.imore.stc.service.model.STCTestEventEB;
import com.knet.imore.stc.service.model.TestEventEB;
import com.knet.imore.stc.service.transformer.TestEventTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import java.util.Optional;

import static com.knet.imore.stc.common.models.ErrorConstant.INTERNAL_SERVER_ERROR;
import static com.knet.imore.stc.common.models.ErrorConstant.SUCCESS;

@Transactional
@Service("testService")
public class TestServiceImpl extends AbstractCrudService<TestEventEB, TestEventDTO, String> implements TestService {

    @Autowired
    private TestEventDAO testEventDAO;

    @Autowired
    private EventCallbackService eventCallbackService;

    @Autowired
    private TestEventTransformer testEventTransformer;

    @Override
    @Async("threadPoolTaskExecutor")
    public void processTestEvent(TestEventDTO testEventDTO) {
        STCTestEventEB stcTestEventEB = new STCTestEventEB();
        try {
            stcTestEventEB.setTestEvent(testEventTransformer.transformDTOToEntity(testEventDTO));
            testEventDAO.save(stcTestEventEB);
            eventCallbackService.generateCallback(stcTestEventEB, EventStatus.SUCCESS, SUCCESS);
        }catch(Exception e){
            logger.error("Error: process test for event with id {} and type {} aborted due to",
                    testEventDTO.getType().getValue(), testEventDTO.getId(), e);
            eventCallbackService.generateCallback(stcTestEventEB, EventStatus.ERROR, INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    @Async("threadPoolTaskExecutor")
    public Optional<BasicModel> expireTestEvent(TestEventDTO testEventDTO){
        return Optional.empty();
    }
}
