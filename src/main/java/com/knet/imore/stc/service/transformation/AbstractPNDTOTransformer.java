package com.knet.imore.stc.service.transformation;

import com.knet.imore.stc.common.dto.DTO;
import com.knet.imore.stc.common.dto.pn.BasicModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.Set;

public class AbstractPNDTOTransformer<T extends BasicModel, D extends DTO> implements PNDTOTransformer<T, D> {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    protected DozerMapper dozerMapper;
    private Class<T> modelClass;
    private Class<D> dtoClass;

    public AbstractPNDTOTransformer(Class<T> modelClass, Class<D> dtoClass) {
        this.modelClass = modelClass;
        this.dtoClass = dtoClass;
    }

    // ********************** Entity TO DTO transformation methods ****************************

    @Override
    public D transformModelToDTO(T sourceModel) {
        D destinationDTO = (D) dozerMapper.map(sourceModel, dtoClass);
        doTransformModelToDTO(sourceModel, destinationDTO);
        return destinationDTO;

    }

    @Override
    public void transformModelToDTO(T sourceModel, D destinationDTO) {
        dozerMapper.map(sourceModel, destinationDTO);
        doTransformModelToDTO(sourceModel, destinationDTO);

    }

    @Override
    public Set<D> transformModelToDTO(Set<T> models) {
        Set<D> dtos = new HashSet<>();
        for (T model : models) {
            dtos.add(transformModelToDTO(model));
        }
        return dtos;

    }

    // ********************** DTO TO BasicModel transformation methods ****************************

    @Override
    public void transformDTOToModel(D sourceDTO, T destinationModel) {
        dozerMapper.map(sourceDTO, destinationModel);
        doTransformDTOToModel(sourceDTO, destinationModel);
    }

    @Override
    public T transformDTOToModel(D sourceDTO) {
        T destinationModel = (T) dozerMapper.map(sourceDTO, modelClass);
        doTransformDTOToModel(sourceDTO, destinationModel);
        return destinationModel;
    }

    @Override
    public Set<T> transformDTOToModel(Set<D> dtos) {
        Set<T> models = new HashSet<>();
        for (D dto : dtos) {
            models.add(transformDTOToModel(dto));
        }
        return models;

    }

    // ********************** Customize Methods ****************************

    protected void doTransformModelToDTO(T sourceModel, D destinationDTO) {
        // to be implemented to custom
    }

    protected void doTransformDTOToModel(D sourceDTO, T destinationModel) {
        // to be implemented to custom
    }
}
