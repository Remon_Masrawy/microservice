package com.knet.imore.stc.service.model;

import java.io.Serializable;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public interface Entity<PK extends Serializable> extends Serializable {

    public abstract PK pk();
}
