package com.knet.imore.stc.service.dao;

import com.knet.imore.stc.service.model.STCTestEventEB;

public interface TestEventDAO extends AbstractDao<STCTestEventEB, Long> {
}
