package com.knet.imore.stc.service.transformer;

import com.knet.imore.stc.common.dto.SubscriptionEventDTO;
import com.knet.imore.stc.service.model.SubscriptionEventEB;
import com.knet.imore.stc.service.transformation.AbstractDTOTransformer;
import org.springframework.stereotype.Component;

@Component("subscriptionEventTransformer")
public class SubscriptionEventTransformerImpl extends AbstractDTOTransformer<SubscriptionEventEB, SubscriptionEventDTO>
        implements SubscriptionEventTransformer {

    public SubscriptionEventTransformerImpl(){
        super(SubscriptionEventEB.class, SubscriptionEventDTO.class);
    }
}
