package com.knet.imore.stc.service.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import java.util.Set;

@DynamicInsert
@DynamicUpdate
@Getter
@Setter
public class SubscriptionUserEB extends AbstractEntity<Long> {

    private Long id;
    private Long subscriptionId;
    private Long userId;
    @Type(type = "json")
    private Set<String> roles;
    @Type(type = "json")
    private UserEB user;
    @Override
    public Long pk() {
        return id;
    }
}
