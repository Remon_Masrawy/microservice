package com.knet.imore.stc.service.services;

import com.knet.imore.stc.common.dto.SubscriptionEventDTO;
import com.knet.imore.stc.common.dto.pn.BasicModel;
import com.knet.imore.stc.service.model.SubscriptionEventEB;

import java.util.Optional;

public interface SubscriptionService extends CrudService<SubscriptionEventEB, SubscriptionEventDTO, String> {

    public void processSubscription(SubscriptionEventDTO subscriptionEventDTO);
    public Optional<BasicModel> expireSubscription(SubscriptionEventDTO subscriptionEventDTO);
}
