package com.knet.imore.stc.service.audit;

import javax.persistence.Embedded;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.OptimisticLock;

@MappedSuperclass
public class AbstractAuditingEntity {

    @Embedded
    @OptimisticLock(excluded=true)
    private EntityAudit audit;

    public EntityAudit getAudit() {
        return audit;
    }

    public void setAudit(EntityAudit audit) {
        this.audit = audit;
    }
}
