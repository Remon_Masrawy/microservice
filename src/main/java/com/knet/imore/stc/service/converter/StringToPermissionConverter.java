package com.knet.imore.stc.service.converter;

import com.knet.imore.stc.common.dto.pn.PermissionModel;
import org.dozer.CustomConverter;
import org.dozer.DozerConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Qualifier("stringToPermissionConverter")
public class StringToPermissionConverter extends DozerConverter<Set<String>, List<PermissionModel>> implements CustomConverter {

    public StringToPermissionConverter(){
        super((Class<Set<String>>) (Class) Set.class, (Class<List<PermissionModel>>) (Class) List.class);
    }
    @Override
    public List<PermissionModel> convertTo(Set<String> strings, List<PermissionModel> permissionModels) {
        if(!Optional.ofNullable(strings).isPresent()){
            return new ArrayList<>();
        }
        return strings.stream().map(PermissionModel::new).collect(Collectors.toList());
    }

    @Override
    public Set<String> convertFrom(List<PermissionModel> permissionModels, Set<String> strings) {
        return null;
    }
}
