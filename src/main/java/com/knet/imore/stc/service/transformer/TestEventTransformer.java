package com.knet.imore.stc.service.transformer;

import com.knet.imore.stc.common.dto.TestEventDTO;
import com.knet.imore.stc.service.model.TestEventEB;
import com.knet.imore.stc.service.transformation.DTOTransformer;

public interface TestEventTransformer extends DTOTransformer<TestEventEB, TestEventDTO> {
}
