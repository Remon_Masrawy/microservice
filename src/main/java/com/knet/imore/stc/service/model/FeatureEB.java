package com.knet.imore.stc.service.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import java.time.LocalDateTime;
import java.util.Map;

@DynamicInsert
@DynamicUpdate
@Getter
@Setter
public class FeatureEB extends AbstractEntity<Long> {

    private Long id;
    private String name;
    private String description;
    private LocalDateTime created;
    private LocalDateTime modified;
    private Map<String, String> metaData;

    @Override
    public Long pk() {
        return id;
    }
}
