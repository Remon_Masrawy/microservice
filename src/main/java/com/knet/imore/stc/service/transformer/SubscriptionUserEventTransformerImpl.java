package com.knet.imore.stc.service.transformer;

import com.knet.imore.stc.common.dto.SubscriptionEventDTO;
import com.knet.imore.stc.common.dto.SubscriptionUserEventDTO;
import com.knet.imore.stc.service.model.SubscriptionEventEB;
import com.knet.imore.stc.service.model.SubscriptionUserEventEB;
import com.knet.imore.stc.service.transformation.AbstractDTOTransformer;
import org.springframework.stereotype.Component;

@Component("subscriptionUserEventTransformer")
public class SubscriptionUserEventTransformerImpl
        extends AbstractDTOTransformer<SubscriptionUserEventEB, SubscriptionUserEventDTO>
        implements SubscriptionUserEventTransformer {

    public SubscriptionUserEventTransformerImpl(){
        super(SubscriptionUserEventEB.class, SubscriptionUserEventDTO.class);
    }
}
