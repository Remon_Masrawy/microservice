package com.knet.imore.stc.service.services;

import com.knet.imore.stc.common.dto.StringExtraFieldDTO;
import com.knet.imore.stc.common.dto.SubscriptionEventDTO;
import com.knet.imore.stc.common.dto.pn.*;
import com.knet.imore.stc.common.exceptions.SubscriptionCreationException;
import com.knet.imore.stc.common.exceptions.SubscriptionCreationExpiredException;
import com.knet.imore.stc.common.exceptions.SubscriptionUpdateException;
import com.knet.imore.stc.common.models.EventStatus;
import com.knet.imore.stc.common.models.Gender;
import com.knet.imore.stc.common.models.UserType;
import com.knet.imore.stc.service.dao.SubscriptionEventDAO;
import com.knet.imore.stc.service.management.TokenGenerator;
import com.knet.imore.stc.service.model.STCSubscriptionEventEB;
import com.knet.imore.stc.service.model.SubscriptionEventEB;
import com.knet.imore.stc.service.transformer.PNSubscriptionEventModelTransformer;
import com.knet.imore.stc.service.transformer.PNSubscriptionEventTransformer;
import com.knet.imore.stc.service.transformer.SubscriptionEventTransformer;
import com.knet.imore.stc.service.utils.DigestUtils;
import com.knet.imore.stc.service.utils.HelperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.security.DigestException;
import java.util.Optional;

import static com.knet.imore.stc.common.models.ErrorConstant.INTERNAL_SERVER_ERROR;

@Transactional
@Service("subscriptionService")
public class SubscriptionServiceImpl extends AbstractCrudService<SubscriptionEventEB, SubscriptionEventDTO, String>
        implements SubscriptionService {

    @Autowired
    private HttpService httpService;

    @Autowired
    private EventCallbackService eventCallbackService;

    @Autowired
    private SubscriptionEventDAO subscriptionEventDAO;

    @Autowired
    private SubscriptionEventTransformer subscriptionEventTransformer;

    @Autowired
    private PNSubscriptionEventModelTransformer pnSubscriptionEventModelTransformer;

    @Value("${extra-field.company.key:company_key}")
    private String extraFieldCompanyKey;

    @Value("${cloneable.organization.id:4080}")
    private Long cloneableOrganizationId;

    @Value("${preferred.language:en}")
    private String preferredLanguage;

    @Override
    @Async("threadPoolTaskExecutor")
    public void processSubscription(SubscriptionEventDTO subscriptionEventDTO) {
        logger.debug("Begin processing subscription:{}", HelperUtils.dumpObjectToJson(subscriptionEventDTO));
        STCSubscriptionEventEB stcSubscriptionEventEB = new STCSubscriptionEventEB();
        try{
            stcSubscriptionEventEB.setSubscriptionEvent(subscriptionEventTransformer.
                    transformDTOToEntity(subscriptionEventDTO));
            subscriptionEventDAO.save(stcSubscriptionEventEB);
            Optional<BasicModel> basicModel = Optional.empty();
            switch(subscriptionEventDTO.getType()){
                case SUBSCRIPTION_CREATED:
                    basicModel = createSubscription(subscriptionEventDTO);
                    break;
                case SUBSCRIPTION_RESUMED:
                case SUBSCRIPTION_CANCELED:
                case SUBSCRIPTION_SUSPENDED:
                case SUBSCRIPTION_PLAN_CHANGED:
                    basicModel = updateSubscription(subscriptionEventDTO);
                    break;
            }
            eventCallbackService.handleCallback(basicModel.get(), stcSubscriptionEventEB);
        }catch(SubscriptionCreationException | SubscriptionUpdateException e){
            eventCallbackService.generateCallback(stcSubscriptionEventEB, EventStatus.ERROR, INTERNAL_SERVER_ERROR);
        }catch(Exception e){
            logger.error("Error: process subscription user for event with id {} and type {} aborted due to",
                    subscriptionEventDTO.getId(), subscriptionEventDTO.getType().getValue(), e);
            eventCallbackService.generateCallback(stcSubscriptionEventEB, EventStatus.ERROR, INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public Optional<BasicModel> expireSubscription(SubscriptionEventDTO subscriptionEventDTO){
        Optional<BasicModel> basicModel = Optional.empty();
        switch (subscriptionEventDTO.getType()) {
            case SUBSCRIPTION_CREATED:
                basicModel = expireCreateSubscription(subscriptionEventDTO);
                break;
            case SUBSCRIPTION_RESUMED:
            case SUBSCRIPTION_CANCELED:
            case SUBSCRIPTION_SUSPENDED:
            case SUBSCRIPTION_PLAN_CHANGED:
                basicModel = expireUpdateSubscription(subscriptionEventDTO);
                break;
        }
        return basicModel;
    }

    private Optional<BasicModel> createSubscription(SubscriptionEventDTO subscriptionEventDTO){
        RegistrationModel registrationModel = constructRegistrationModel(subscriptionEventDTO);
        HttpEntity<RegistrationModel> entity = new HttpEntity<>(registrationModel, httpService.generateHeaders());
        String url = new StringBuilder()
                .append("PRN/PRN/PRN/onlineRegistration/register/")
                .append(cloneableOrganizationId)
                .append("/0")
                .toString();
        try {
            Optional<BasicModel> basicModel = httpService.sendRequest(url, entity, HttpMethod.POST);
            return basicModel;
        }catch(Exception e){
            logger.error("Error: error occurred while trying to subscribe user to service due to", e);
            throw new SubscriptionCreationException();
        }
    }

    private Optional<BasicModel> expireCreateSubscription(SubscriptionEventDTO subscriptionEventDTO){
        HttpEntity<Void> entity = new HttpEntity<>(httpService.generateHeaders());
        String url = "PRN/PRN/PRN/subscription/cancelSubscription/{subscriptionId}"
                .replace("{subscriptionId}", String.valueOf(subscriptionEventDTO.getSubscriptionDTO().getId()));
        try {
            Optional<BasicModel> basicModel = httpService.sendRequest(url, entity, HttpMethod.DELETE);
            return basicModel;
        }catch(Exception e){
            logger.error("Error: error occurred while trying to expire subscribe user to service due to", e);
            throw new SubscriptionCreationExpiredException();
        }
    }

    private Optional<BasicModel> updateSubscription(SubscriptionEventDTO subscriptionEventDTO){
        SubscriptionModel subscriptionModel = pnSubscriptionEventModelTransformer.transformDTOToModel(subscriptionEventDTO);
        HttpEntity<SubscriptionModel> entity = new HttpEntity<>(subscriptionModel, httpService.generateHeaders());
        String url = "PRN/PRN/PRN/subscription/updateSubscription";
        try {
            Optional<BasicModel> basicModel = httpService.sendRequest(url, entity, HttpMethod.PUT);
            return basicModel;
        }catch(Exception e){
            logger.error("Error: error occurred while trying to update subscription due to", e);
            throw new SubscriptionUpdateException();
        }
    }

    private Optional<BasicModel> expireUpdateSubscription(SubscriptionEventDTO subscriptionEventDTO){
        return Optional.empty();
    }

    private RegistrationModel constructRegistrationModel(SubscriptionEventDTO subscriptionEventDTO){
        RegistrationModel registrationModel = new RegistrationModel();
        /************ Company Model **************/
        // Check existence of company value else send reject status
        String company = ((StringExtraFieldDTO) subscriptionEventDTO.getSubscriptionDTO()
                .getExtraFields()
                .get(extraFieldCompanyKey))
                .getValue();
        CompanyFullModel companyModel = new CompanyFullModel();
        companyModel.setCode(company);
        companyModel.setOrganizationId(cloneableOrganizationId);
        // Throws Exception
        String currency = subscriptionEventDTO.getSubscriptionDTO().getPrice().getCurrency();
        companyModel.setCurrencyCode(currency);
        companyModel.setAdminUnitName(company);
        /************ Contact Info Model **************/
        // Call UserInfo endpoint
        // Extra Field for email, username, mobile number
        ContactInfoModel contactInfoModel = new ContactInfoModel();
        contactInfoModel.setEmail("hard.coded@pn.com");
        contactInfoModel.setContactName("hard.coded");
        contactInfoModel.setMobileNo("01203006090");
        companyModel.setContactInfoModel(contactInfoModel);
        /************ Address Model **************/
        // Setup Extra Field for Address on STC
        AddressModel addressModel = new AddressModel();
        companyModel.setAddressModel(addressModel);
        // Setup Extra Field for primary language
        // Setup Quantifiable Item for Number of users
        registrationModel.setCompanyModel(companyModel);
        registrationModel.setNoOfUsers(1000);
        try {
            String context = new StringBuilder().append("org_").append(DigestUtils.generateUUID()).toString();
            registrationModel.setContext(context);
        }catch(DigestException ex){
            logger.error("Error: cannot generate UUID for context.");
            // Terminate process and send callback with error status
        }
        /************ Admin User Model **************/
        UserModel adminUser = new UserModel();
        adminUser.setType(UserType.ORG_ADMIN);
        String username = new StringBuilder().append("orgadmin").append(registrationModel.getContext()).toString();
        adminUser.setUsername(username);
        try {
            String email = new StringBuilder().append(DigestUtils.generateUUID()).append("@pn.com").toString();
            adminUser.setEmail(email);
        }catch(DigestException ex){
            logger.error("Error: cannot generate UUID for email.");
            // Terminate process and send callback with error status
        }
        // Setup Extra Field select type for language
        adminUser.setPreferredLanguage(preferredLanguage);
        // Setup Extra Field select type for gender
        adminUser.setGender(Gender.MALE);
        adminUser.setFullEnglishName("First name");
        registrationModel.setAdminUser(adminUser);
        /************ End User Model **************/
        UserModel endUser = new UserModel();
        endUser.setType(UserType.NORMAL_USER);
        endUser.setEmail("remon.g.azmy@gmail.com");
        endUser.setUsername("First name");
        endUser.setMobileNumber("01203006090");
        endUser.setPreferredLanguage(preferredLanguage);
        endUser.setGender(Gender.MALE);
        endUser.setFullEnglishName("First name");
        registrationModel.setEndUser(endUser);
        /************ Subscription Model **************/
        SubscriptionModel subscriptionModel = new SubscriptionModel();
        subscriptionModel.setId(0);
        // Setup Extra Field select type for country
        subscriptionModel.setCountryCode("SAR");
        subscriptionModel.setSubscriptionPlan(subscriptionEventDTO.getSubscriptionDTO().getPlan().getName());
        subscriptionModel.setPaymentTerm(subscriptionEventDTO.getSubscriptionDTO().getPrice().getPeriodUnit());
        // Setup Quantifiable Item for Number of Units
        subscriptionModel.setNoOfUnits(200);
        // Throws Exception
        try {
            subscriptionModel.setExpiryDate(subscriptionEventDTO.getSubscriptionDTO().getEndDate().toString());
        }catch(Exception e){
            logger.error("WARN: cannot find end date on subscription object.");
        }
        subscriptionModel.setUserName("First Name");
        subscriptionModel.setEmail("remon.g.azmy@gmail.com");
        subscriptionModel.setCompanyName(company);
        subscriptionModel.setSubscriptionId(subscriptionEventDTO.getSubscriptionDTO().getId());
        subscriptionModel.setStatus(subscriptionEventDTO.getType());

        registrationModel.setSubscriptionModel(subscriptionModel);
        return registrationModel;
    }
}