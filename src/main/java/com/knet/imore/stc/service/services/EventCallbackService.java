package com.knet.imore.stc.service.services;

import com.knet.imore.stc.common.dto.EventCallbackDTO;
import com.knet.imore.stc.common.dto.pn.BasicModel;
import com.knet.imore.stc.common.models.EventStatus;
import com.knet.imore.stc.service.model.EventCallbackEB;
import com.knet.imore.stc.service.model.STCEventEB;

public interface EventCallbackService extends CrudService<EventCallbackEB, EventCallbackDTO, String> {

    public void handleCallback(BasicModel basicModel, STCEventEB stcEventEB);
    public void generateCallback(STCEventEB stcEventEB, EventStatus status, String msg);
}
