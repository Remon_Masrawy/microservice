package com.knet.imore.stc.service.transformer;

import com.knet.imore.stc.common.dto.CustomerEventDTO;
import com.knet.imore.stc.service.model.CustomerEventEB;
import com.knet.imore.stc.service.transformation.AbstractDTOTransformer;
import org.springframework.stereotype.Component;

@Component("customerEventTransformer")
public class CustomerEventTransformerImpl extends AbstractDTOTransformer<CustomerEventEB, CustomerEventDTO>
        implements CustomerEventTransformer {

    public CustomerEventTransformerImpl(){
        super(CustomerEventEB.class, CustomerEventDTO.class);
    }
}
