package com.knet.imore.stc.service.transformer;

import com.knet.imore.stc.common.dto.SubscriptionUserEventDTO;
import com.knet.imore.stc.common.dto.pn.RegistrationModel;
import com.knet.imore.stc.service.transformation.AbstractPNDTOTransformer;
import org.springframework.stereotype.Component;

@Component("pnSubscriptionUserEventTransformer")
public class PNSubscriptionUserEventTransformerImpl
        extends AbstractPNDTOTransformer<RegistrationModel, SubscriptionUserEventDTO>
        implements PNSubscriptionUserEventTransformer {

    public PNSubscriptionUserEventTransformerImpl(){
        super(RegistrationModel.class, SubscriptionUserEventDTO.class);
    }
}
