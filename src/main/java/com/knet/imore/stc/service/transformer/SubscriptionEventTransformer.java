package com.knet.imore.stc.service.transformer;

import com.knet.imore.stc.common.dto.SubscriptionEventDTO;
import com.knet.imore.stc.service.model.SubscriptionEventEB;
import com.knet.imore.stc.service.transformation.DTOTransformer;

public interface SubscriptionEventTransformer extends DTOTransformer<SubscriptionEventEB, SubscriptionEventDTO> {
}
