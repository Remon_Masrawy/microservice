package com.knet.imore.stc.service.dao;

import java.io.Serializable;
import java.util.Optional;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.knet.imore.stc.service.model.Entity;

@NoRepositoryBean
public interface AbstractDao<E extends Entity<?>, ID extends Serializable> extends PagingAndSortingRepository<E, ID> {
    boolean existsByEventId(String id);
}
