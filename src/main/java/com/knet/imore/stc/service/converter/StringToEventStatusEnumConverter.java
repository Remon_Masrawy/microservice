package com.knet.imore.stc.service.converter;

import org.springframework.core.convert.converter.Converter;

import com.knet.imore.stc.common.models.EventStatus;

public class StringToEventStatusEnumConverter implements Converter<String, EventStatus> {

    @Override
    public EventStatus convert(String status) {
        return EventStatus.valueOf(status);
    }
}
