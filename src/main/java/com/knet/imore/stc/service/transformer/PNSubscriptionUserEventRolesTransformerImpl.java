package com.knet.imore.stc.service.transformer;

import com.knet.imore.stc.common.dto.SubscriptionUserEventDTO;
import com.knet.imore.stc.common.dto.pn.RoleModel;
import com.knet.imore.stc.service.transformation.AbstractPNDTOTransformer;
import org.springframework.stereotype.Component;

@Component("pnSubscriptionUserEventRolesTransformer")
public class PNSubscriptionUserEventRolesTransformerImpl
        extends AbstractPNDTOTransformer<RoleModel, SubscriptionUserEventDTO>
        implements PNSubscriptionUserEventRolesTransformer {

    public PNSubscriptionUserEventRolesTransformerImpl(){
        super(RoleModel.class, SubscriptionUserEventDTO.class);
    }
}
