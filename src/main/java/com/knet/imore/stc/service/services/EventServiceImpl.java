package com.knet.imore.stc.service.services;

import com.knet.imore.stc.common.dto.*;
import com.knet.imore.stc.service.dao.EventDAO;
import com.knet.imore.stc.service.model.EventEB;
import com.knet.imore.stc.service.model.STCEventEB;
import com.knet.imore.stc.service.transformer.EventTransformer;
import com.knet.imore.stc.service.utils.HelperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Transactional
@Service("eventService")
public class EventServiceImpl extends AbstractCrudService<EventEB, EventDTO, String> implements EventService {

    @Autowired
    private EventDAO eventDAO;
    @Autowired
    private TestService testService;
    @Autowired
    private ExpiredService expiredService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private SubscriptionService subscriptionService;
    @Autowired
    private SubscriptionUserService subscriptionUserService;

    @Override
    @Async("threadPoolTaskExecutor")
    public void processEvent(EventDTO eventDTO){
        logger.debug("Begin processing incoming event:{} with type:{}", eventDTO.getId(), eventDTO.getType());
        if(isEventAlreadyReceived(eventDTO)){
            logger.debug("Received duplicate of an exists one, so reply with 200 OK. ");
            return;
        }
        switch(HelperUtils.findEventCategory(eventDTO.getType())){
            case SUBSCRIPTION_EVENT:
                subscriptionService.processSubscription((SubscriptionEventDTO) eventDTO);
                break;
            case CUSTOMER_EVENT:
                customerService.processCustomer((CustomerEventDTO) eventDTO);
                break;
            case SUBSCRIPTION_USER_EVENT:
                subscriptionUserService.processSubscriptionUser((SubscriptionUserEventDTO) eventDTO);
                break;
            case TEST_EVENT:
                testService.processTestEvent((TestEventDTO) eventDTO);
                break;
            case EXPIRED_EVENT:
                expiredService.expireEvent((ExpiredEventDTO) eventDTO);
                break;
            case UNDEFINED_EVENT:
        }
        logger.debug("End processing incoming event:{}", eventDTO.getId());
    }

    private boolean isEventAlreadyReceived(EventDTO eventDTO){
        return eventDAO.existsByEventId(eventDTO.getId());
    }
}
