package com.knet.imore.stc.service.services;

import com.knet.imore.stc.common.dto.EventCallbackDTO;
import com.knet.imore.stc.common.dto.STCCallbackResponse;
import com.knet.imore.stc.common.dto.pn.BasicModel;
import com.knet.imore.stc.common.models.EventStatus;
import com.knet.imore.stc.service.dao.EventDAO;
import com.knet.imore.stc.service.model.EventCallbackEB;
import com.knet.imore.stc.service.model.STCEventEB;
import com.knet.imore.stc.service.transformer.EventCallbackTransformer;
import com.knet.imore.stc.service.utils.HelperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import static com.knet.imore.stc.common.models.ErrorConstant.INTERNAL_SERVER_ERROR;
import static com.knet.imore.stc.common.models.ErrorConstant.SUCCESS;

@Service("eventCallbackService")
public class EventCallbackServiceImpl extends AbstractCrudService<EventCallbackEB, EventCallbackDTO, String>
        implements EventCallbackService {

    @Autowired
    private EventDAO eventDAO;

    @Autowired
    private HttpService httpService;

    @Autowired
    private EventCallbackTransformer eventCallbackTransformer;

    @Override
    public void handleCallback(BasicModel basicModel, STCEventEB stcEventEB){
        if(HttpStatus.valueOf(basicModel.getReplyCode()).is2xxSuccessful())
            this.generateCallback(stcEventEB, EventStatus.SUCCESS, SUCCESS);
        else if(HttpStatus.valueOf(basicModel.getReplyCode()).is4xxClientError())
            this.generateCallback(stcEventEB, EventStatus.ERROR, INTERNAL_SERVER_ERROR);
        else if(HttpStatus.valueOf(basicModel.getReplyCode()).is5xxServerError()){
            this.generateCallback(stcEventEB, EventStatus.REJECT, INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public void generateCallback(STCEventEB stcEventEB, EventStatus status, String msg){
        logger.debug("Begin generating callback to event {}, with status {} and msg {}",
                stcEventEB.getEventId(), status.getValue(), msg);
        EventCallbackEB eventCallbackEB = generateCallbackEntity(stcEventEB, status, msg);
        EventCallbackDTO eventCallbackDTO;
        STCCallbackResponse stcCallbackResponse;
        try {
            stcEventEB.setEventCallbackEB(eventCallbackEB);
            eventCallbackDTO = eventCallbackTransformer.transformEntityToDTO(eventCallbackEB);
            try {
                stcCallbackResponse = httpService.sendCallback(eventCallbackDTO, stcEventEB.getEventId());
            } catch (Exception e) {
                logger.error("Error: error sending callback reply to event:{} and internal id:{}",
                        stcEventEB.getEventId(), stcEventEB.pk(), e);
                throw e;
            }
            eventCallbackEB.setResponse(stcCallbackResponse);
            // Validate Response
            logger.debug("End sending callback with response:{}", HelperUtils.dumpObjectToJson(stcCallbackResponse));
        }catch(Exception e){
            logger.error("Error: error occurred while trying to generate callback to event {} with status {}",
                    stcEventEB.getEventId(), status, e);
            stcCallbackResponse = new STCCallbackResponse(HttpStatus.PRECONDITION_FAILED, e.getMessage());
            eventCallbackEB.setResponse(stcCallbackResponse);
        }finally {
            try {
                eventDAO.save(stcEventEB);
            } catch (Exception e) {
                logger.error("Error: cannot save event to database due to", e);
            }
        }
    }

    private EventCallbackEB generateCallbackEntity(STCEventEB stcEventEB, EventStatus status, String msg){
        EventCallbackEB eventCallbackEB = new EventCallbackEB();
        eventCallbackEB.setStatus(status);
        eventCallbackEB.setMessage(msg);
        eventCallbackEB.setRefNumber(stcEventEB.pk());
        eventCallbackEB.setLandingPageURL("Landing Page");
        eventCallbackEB.setManagementPageURL("Management Page");
        eventCallbackEB.setInstructions("Instructions");
        return eventCallbackEB;
    }
}