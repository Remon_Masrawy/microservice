package com.knet.imore.stc.service.configuration;

import java.util.Properties;

import javax.sql.DataSource;

import javax.persistence.EntityManagerFactory;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.configuration.ClassicConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.aspectj.EnableSpringConfigured;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import org.hibernate.jpa.HibernatePersistenceProvider;

import org.apache.commons.dbcp2.BasicDataSource;

import com.knet.imore.stc.service.audit.EntityAuditAware;

@Configuration
@Profile("sqlserver")
@EnableJpaRepositories(basePackages = "com.knet.imore.stc.service.dao",
        entityManagerFactoryRef = "entitiesManagerFactory", transactionManagerRef = "entitiesTransactionManager")
@EnableJpaAuditing(auditorAwareRef = "auditAware", modifyOnCreate = false)
@EnableAspectJAutoProxy
@EnableSpringConfigured
@EnableTransactionManagement(proxyTargetClass = true)
public class EntitiesConfigurationSqlServer {

    @Autowired
    private Environment env;

    @Value("${stc.clearDB}")
    private boolean clearDB;

    // @ConfigurationProperties(prefix="datasource.entities")
    @DependsOn(value = "systemInitializer")
    @Bean(name = "dataSource", destroyMethod = "close")
    @Primary
    public DataSource dataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(env.getProperty("imore.db.driver"));
        dataSource.setUrl(env.getProperty("imore.db.url"));
        dataSource.setUsername(env.getProperty("imore.db.user"));
        dataSource.setPassword(env.getProperty("imore.db.password"));
        dataSource.setTestOnBorrow(true);
        dataSource.setTestOnReturn(true);
        dataSource.setTestWhileIdle(true);
        dataSource.setTimeBetweenEvictionRunsMillis(1800000);
        dataSource.setNumTestsPerEvictionRun(3);
        dataSource.setMinEvictableIdleTimeMillis(1800000);
        //dataSource.setValidationQuery(env.getProperty("imore.db.validationQuery"));
        return dataSource;

    }

    @Primary
    @DependsOn(value = "flyway")
    @Bean(name = "entitiesManagerFactory")
    public EntityManagerFactory entitiesManagerFactory() {
        LocalContainerEntityManagerFactoryBean emf = new LocalContainerEntityManagerFactoryBean();
        emf.setDataSource(dataSource());
        emf.setPersistenceProvider(hibernatePersistenceProvider());
        emf.setJpaVendorAdapter(hibernateJpaVendorAdapter());
        emf.setPackagesToScan("com.knet.imore.stc.service.model");
        Properties properties = new Properties();
        properties.setProperty("hibernate.hbm2ddl.auto",env.getProperty("imore.db.creationStrategy"));
        properties.setProperty("hibernate.default_schema", env.getProperty("imore.db.mainScheme"));
        properties.setProperty("hibernate.default_catalog",env.getProperty("imore.db.catalog"));
        properties.setProperty("hibernate.dialect", env.getProperty("imore.db.dialect"));
        properties.setProperty("hibernate.use_nationalized_character_data", "true");
        properties.setProperty("hibernate.show_sql", "false");
        properties.setProperty("hibernate.generate_statistics", "false");
        properties.setProperty("hibernate.format_sql", "true");
        properties.setProperty("hibernate.use_sql_comments", "false");
        properties.setProperty("hibernate.jdbc.batch_size", "100");
        properties.setProperty("hibernate.order_inserts", "true");
        properties.setProperty("hibernate.order_updates", "true");
        properties.setProperty("hibernate.connection.useUnicode", "true");
        properties.setProperty("hibernate.connection.charSet", "UTF-16");
        properties.setProperty("hibernate.connection.characterEncoding", "UTF-16");
        properties.setProperty("hibernate.physical_naming_strategy",
                "com.knet.imore.stc.service.utils.CapitalizedPhysicalNamingStrategyForHibernate5");
        properties.setProperty("jadira.usertype.autoRegisterUserTypes", "true");
        properties.setProperty("jadira.usertype.javaZone", "UTC");
        properties.setProperty("jadira.usertype.databaseZone", "UTC");
        emf.setJpaProperties(properties);
        emf.afterPropertiesSet();
        return emf.getObject();

    }

    @Bean
    public EntityAuditAware auditAware() {
        return new EntityAuditAware();
    }

    @Bean(name = "entitiesTransactionManager")
    @Primary
    public JpaTransactionManager transactionManager() {
        return new JpaTransactionManager();
    }

    @Bean
    public HibernateJpaVendorAdapter hibernateJpaVendorAdapter() {
        return new HibernateJpaVendorAdapter();
    }

    @Bean
    public HibernatePersistenceProvider hibernatePersistenceProvider() {
        return new HibernatePersistenceProvider();
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor persistenceExceptionTranslationPostProcessor() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    @Bean(name = "flyway")
    @DependsOn(value = "dataSource")
    public Flyway flyWay() {
        ClassicConfiguration configuration = new ClassicConfiguration();
        configuration.setDataSource(dataSource());
        configuration.setLocationsAsStrings("classpath:flyway/migration/sqlserver");
        configuration.setBaselineVersionAsString("0");
        configuration.setBaselineOnMigrate(true);
        Flyway flyway = new Flyway(configuration);
        if(clearDB==true)
            flyway.clean();
        flyway.migrate();
        return flyway;
    }

}
