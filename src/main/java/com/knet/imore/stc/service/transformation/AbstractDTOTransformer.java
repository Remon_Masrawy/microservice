package com.knet.imore.stc.service.transformation;

import com.knet.imore.stc.common.dto.DTO;
import com.knet.imore.stc.service.model.Entity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.Set;

public class AbstractDTOTransformer<E extends Entity<?>, D extends DTO> implements DTOTransformer<E, D> {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    protected DozerMapper dozerMapper;
    private Class<E> entityClass;
    private Class<D> dtoClass;

    public AbstractDTOTransformer(Class<E> entityClass, Class<D> dtoClass) {
        this.entityClass = entityClass;
        this.dtoClass = dtoClass;
    }

    // ********************** Entity TO DTO transformation methods ****************************

    @Override
    public D transformEntityToDTO(E sourceEntity) {
        D destinationDTO = (D) dozerMapper.map(sourceEntity, dtoClass);
        doTransformEntityToDTO(sourceEntity, destinationDTO);
        return destinationDTO;

    }

    @Override
    public void transformEntityToDTO(E sourceEntity, D destinationDTO) {
        dozerMapper.map(sourceEntity, destinationDTO);
        doTransformEntityToDTO(sourceEntity, destinationDTO);

    }

    @Override
    public Set<D> transformEntityToDTO(Set<E> entities) {
        Set<D> dtos = new HashSet<>();
        for (E entity : entities) {
            dtos.add(transformEntityToDTO(entity));
        }
        return dtos;

    }

    // ********************** DTO TO Entity transformation methods ****************************

    @Override
    public void transformDTOToEntity(D sourceDTO, E destinationEntity) {
        dozerMapper.map(sourceDTO, destinationEntity);
        doTransformDTOToEntity(sourceDTO, destinationEntity);
    }

    @Override
    public E transformDTOToEntity(D sourceDTO) {
        E destinationEntity = (E) dozerMapper.map(sourceDTO, entityClass);
        doTransformDTOToEntity(sourceDTO, destinationEntity);
        return destinationEntity;
    }

    @Override
    public Set<E> transformDTOToEntity(Set<D> dtos) {
        Set<E> entities = new HashSet<>();
        for (D dto : dtos) {
            entities.add(transformDTOToEntity(dto));
        }
        return entities;

    }

    // ********************** Customize Methods ****************************

    protected void doTransformEntityToDTO(E sourceEntity, D destinationDTO) {
        // to be implemented to custom
    }

    protected void doTransformDTOToEntity(D sourceDTO, E destinationEntity) {
        // to be implemented to custom
    }
}
