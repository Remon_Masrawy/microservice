package com.knet.imore.stc.service.services;

import com.knet.imore.stc.common.dto.EventDTO;
import com.knet.imore.stc.service.model.EventEB;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;

public interface EventService extends CrudService<EventEB, EventDTO, String> {

    public void processEvent(EventDTO eventDTO);
}
