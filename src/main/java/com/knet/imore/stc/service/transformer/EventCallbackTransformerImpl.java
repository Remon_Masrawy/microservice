package com.knet.imore.stc.service.transformer;

import com.knet.imore.stc.common.dto.EventCallbackDTO;
import com.knet.imore.stc.service.model.EventCallbackEB;
import com.knet.imore.stc.service.transformation.AbstractDTOTransformer;
import org.springframework.stereotype.Component;

@Component("eventCallbackTransformer")
public class EventCallbackTransformerImpl extends AbstractDTOTransformer<EventCallbackEB, EventCallbackDTO>
        implements EventCallbackTransformer {

    public EventCallbackTransformerImpl(){
        super(EventCallbackEB.class, EventCallbackDTO.class);
    }
}
