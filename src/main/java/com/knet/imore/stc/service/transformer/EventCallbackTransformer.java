package com.knet.imore.stc.service.transformer;

import com.knet.imore.stc.common.dto.EventCallbackDTO;
import com.knet.imore.stc.service.model.EventCallbackEB;
import com.knet.imore.stc.service.transformation.DTOTransformer;

public interface EventCallbackTransformer extends DTOTransformer<EventCallbackEB, EventCallbackDTO> {
}
