package com.knet.imore.stc.service.services;

import com.knet.imore.stc.common.dto.EventCallbackDTO;
import com.knet.imore.stc.common.dto.STCCallbackResponse;
import com.knet.imore.stc.common.dto.pn.BasicModel;
import com.knet.imore.stc.service.management.TokenGenerator;
import com.knet.imore.stc.service.utils.HelperUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@Component("httpService")
public class HttpServiceImpl implements HttpService {

    @Value("${url.protocol}")
    private String protocol;

    @Value("${pn.app.name:MASTERINFOADMIN}")
    private String pnAppName;

    @Value("${api-gateway.app.name:IMOREGATEWAY}")
    private String apiGatewayAppName;

    @Value("${stc.callback.url}")
    private String stcCallbackUrl;

    @Autowired
    private TokenGenerator tokenGenerator;

    @Autowired
    private OAuth2RestTemplate oAuth2RestTemplate;

    @Autowired
    private RestTemplate restTemplate;

    private static final Logger logger = LoggerFactory.getLogger(HttpServiceImpl.class);


    @Override
    public Optional<BasicModel> sendRequest(String url, HttpEntity entity, HttpMethod method) {
        BasicModel basicModel;
        ResponseEntity<BasicModel> exchange;
        synchronized (this) {
            exchange = restTemplate.exchange(
                    protocol + "://" + pnAppName + "/" + pnAppName + "/" + url,
                    method, entity, BasicModel.class);
        }
        this.checkResponse(exchange);
        basicModel = exchange.getBody();
        return Optional.ofNullable(basicModel);
    }

    @Override
    public STCCallbackResponse sendCallback(EventCallbackDTO eventCallbackDTO, final String eventId) {
        logger.debug("Start sending callback to STC marketplace for event with id:{}", eventId);
        final OAuth2AccessToken accessToken;
        try {
            accessToken = oAuth2RestTemplate.getAccessToken();
        }catch(Exception e){
            logger.error("Error: error getting access token due to", e);
            throw e;
        }
        if (accessToken.isExpired()) {
            logger.error("Error: Token expired on {}", accessToken.getExpiration());
        }
        final String token = accessToken.getValue();
        final String url = stcCallbackUrl.replace("{event_id}", eventId);
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(token);
        HttpEntity<EventCallbackDTO> entity = new HttpEntity<>(eventCallbackDTO, headers);
        ResponseEntity<STCCallbackResponse> exchange;
        synchronized (this) {
            try {
                exchange = oAuth2RestTemplate.exchange(url, HttpMethod.PUT, entity, STCCallbackResponse.class);
            }catch(Exception ex){
                logger.error("Error: error occurred when trying to hit url {} due to ", url, ex);
                throw ex;
            }
        }
        this.checkResponse(exchange);
        STCCallbackResponse stcCallbackResponse = exchange.getBody();
        logger.debug("End sending callback to STC marketplace for event with id:{} and With response:{}",
                eventId, HelperUtils.dumpObjectToJson(stcCallbackResponse));
        return stcCallbackResponse;
    }

    @Override
    public final HttpHeaders generateHeaders(){
        HttpHeaders headers = new HttpHeaders();
        headers.set("token", tokenGenerator.generateHashedToken());
        return headers;
    }

    private <T> void checkResponse(ResponseEntity<T> exchange){
        Optional.of(exchange).orElseThrow(NullPointerException::new);
        if(exchange.getStatusCode().is2xxSuccessful()){
            logger.debug("Successfully call {} and response returned with status {}",
                    pnAppName, HelperUtils.dumpObjectToJson(exchange.getStatusCode()));
        }else if(exchange.getStatusCode().is4xxClientError()){
            logger.error("Error: client error occurred when trying to call {} and response returned with status {}",
                    pnAppName, HelperUtils.dumpObjectToJson(exchange.getStatusCode()));
            throw new HttpClientErrorException(exchange.getStatusCode());
        }else if(exchange.getStatusCode().is5xxServerError()){
            logger.error("Error: Server error occurred when trying to call {} and response returned with status {}",
                    pnAppName, HelperUtils.dumpObjectToJson(exchange.getStatusCode()));
            throw new HttpServerErrorException(exchange.getStatusCode());
        }
    }
}