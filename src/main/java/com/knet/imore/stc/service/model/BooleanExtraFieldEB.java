package com.knet.imore.stc.service.model;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

@DynamicInsert
@DynamicUpdate
@Getter
@Setter
public class BooleanExtraFieldEB extends ExtraFieldEB {

    private Boolean value;
}
