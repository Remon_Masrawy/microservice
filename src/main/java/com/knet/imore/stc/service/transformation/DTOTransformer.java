package com.knet.imore.stc.service.transformation;

import com.knet.imore.stc.common.dto.DTO;
import com.knet.imore.stc.service.model.Entity;

import java.util.Set;

public interface DTOTransformer<E extends Entity<?>, D extends DTO> {

    // ********************** Entity TO DTO transformation methods ****************************
    public D transformEntityToDTO(E sourceEntity);

    public void transformEntityToDTO(E sourceEntity, D destinationDTO);

    public Set<D> transformEntityToDTO(Set<E> entities);

    // ********************** DTO TO Entity transformation methods ****************************
    public void transformDTOToEntity(D sourceDTO, E destinationEntity);

    public E transformDTOToEntity(D sourceDTO);

    public Set<E> transformDTOToEntity(Set<D> dtos);
}
