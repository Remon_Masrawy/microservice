package com.knet.imore.stc.service.model;

import com.knet.imore.stc.common.models.PlanType;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;

@DynamicInsert
@DynamicUpdate
@Getter
@Setter
public class PlanEB extends AbstractEntity<Long> {

    private Long id;
    private String name;
    private String description;
    @Type(type = "json")
    private PriceEB initialPrice;
    @Type(type = "json")
    private Set<PlanItemEB> planItems;
    @Type(type = "json")
    private Set<FeatureEB> features;
    @Type(type = "json")
    private Set<PriceEB> prices;
    private Map<String, String> metaData;
    private LocalDateTime created;
    private LocalDateTime modified;

    @Enumerated(EnumType.STRING)
    private PlanType type;
    private String maximumAllowedSubscriptions;

    @Override
    public Long pk() {
        return id;
    }
}
