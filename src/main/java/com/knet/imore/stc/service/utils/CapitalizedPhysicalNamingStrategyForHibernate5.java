package com.knet.imore.stc.service.utils;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;
import org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy;

import java.util.Locale;

public class CapitalizedPhysicalNamingStrategyForHibernate5 extends SpringPhysicalNamingStrategy {



    @Override
    public Identifier toPhysicalColumnName(Identifier name, JdbcEnvironment context) {
        name =  super.toPhysicalColumnName(name ,context);
        if (name == null) {
            return null;
        }
        return context.getIdentifierHelper().toIdentifier(name.getText().toUpperCase(Locale.ENGLISH));
    }

    @Override
    public Identifier toPhysicalTableName(Identifier name, JdbcEnvironment jdbcEnvironment) {
        name = super.toPhysicalTableName(name, jdbcEnvironment);
        if (name == null) {
            return null;
        }
        return jdbcEnvironment.getIdentifierHelper().toIdentifier(name.getText().toUpperCase(Locale.ENGLISH));
    }

    @Override
    public Identifier toPhysicalCatalogName(Identifier name, JdbcEnvironment jdbcEnvironment) {
        name =  super.toPhysicalCatalogName(name, jdbcEnvironment);
        if (name == null) {
            return null;
        }
        return jdbcEnvironment.getIdentifierHelper().toIdentifier(name.getText().toUpperCase(Locale.ENGLISH));
    }

    @Override
    public Identifier toPhysicalSchemaName(Identifier name, JdbcEnvironment jdbcEnvironment) {
        name = super.toPhysicalSchemaName(name, jdbcEnvironment);
        if (name == null) {
            return null;
        }
        return jdbcEnvironment.getIdentifierHelper().toIdentifier(name.getText().toUpperCase(Locale.ENGLISH));
    }

    @Override
    public Identifier toPhysicalSequenceName(Identifier name, JdbcEnvironment jdbcEnvironment) {
        name=  super.toPhysicalSequenceName(name, jdbcEnvironment);
        if (name == null) {
            return null;
        }
        return jdbcEnvironment.getIdentifierHelper().toIdentifier(name.getText().toUpperCase(Locale.ENGLISH));
    }
}
