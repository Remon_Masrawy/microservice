package com.knet.imore.stc.service.utils;

import com.google.gson.Gson;
import com.knet.imore.stc.common.models.EventCategory;
import com.knet.imore.stc.common.models.EventType;

public class HelperUtils {

    private static final Gson gson = new Gson();

    public static EventCategory findEventCategory(EventType eventType){
        switch(eventType){
            case SUBSCRIPTION_CREATED:
            case SUBSCRIPTION_SUSPENDED:
            case SUBSCRIPTION_RESUMED:
            case SUBSCRIPTION_CANCELED:
            case SUBSCRIPTION_PLAN_CHANGED:
            case SUBSCRIPTION_ADDON_ATTACHED:
            case SUBSCRIPTION_ADDON_CANCELED:
                return EventCategory.SUBSCRIPTION_EVENT;
            case SUBSCRIPTION_USER_ADDED:
            case SUBSCRIPTION_USER_REMOVED:
            case SUBSCRIPTION_USER_ROLES_CHANGED:
                return EventCategory.SUBSCRIPTION_USER_EVENT;
            case ACCOUNT_SUSPENDED:
            case ACCOUNT_RESUMED:
            case ACCOUNT_TRIAL_FIRST_SUSPENDED:
            case ACCOUNT_TRIAL_SECOND_SUSPENDED:
            case ACCOUNT_TRIAL_FINAL_SUSPENDED:
            case ACCOUNT_BILLING_ACTIVATED:
            case ACCOUNT_TERMINATED:
                return EventCategory.CUSTOMER_EVENT;
            case WEBHOOK_TEST:
                return EventCategory.TEST_EVENT;
            case EVENT_EXPIRED:
                return EventCategory.EXPIRED_EVENT;
            default:
                return EventCategory.UNDEFINED_EVENT;
        }
    }

    public static String dumpObjectToJson(Object object){
        return gson.toJson(object);
    }
}
