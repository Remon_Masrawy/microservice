package com.knet.imore.stc.service.dao;

import com.knet.imore.stc.service.model.STCSubscriptionEventEB;
import com.knet.imore.stc.service.model.STCSubscriptionUserEventEB;

public interface SubscriptionUserEventDAO extends AbstractDao<STCSubscriptionUserEventEB, Long> {
}
