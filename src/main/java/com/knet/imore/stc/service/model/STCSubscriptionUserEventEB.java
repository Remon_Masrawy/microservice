package com.knet.imore.stc.service.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.PrePersist;

@Entity
@DynamicInsert
@DynamicUpdate
@Getter
@Setter
@DiscriminatorValue(value = "SUBSCRIPTION_USER_EVENT")
public class STCSubscriptionUserEventEB extends STCEventEB {

    @Type(type = "json-descriptor")
    @Column(name = "BODY", length = 2000)
    private SubscriptionUserEventEB subscriptionUserEvent;

    @PrePersist
    public void prePersist(){
        this.setEventId(subscriptionUserEvent.pk());
    }
    @Override
    public String getEventId() {return subscriptionUserEvent.getId();}
}
