package com.knet.imore.stc.service.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.persistence.Entity;

@Setter
@Getter
@Entity
@Table(name="STC_EVENT")
@DynamicInsert
@DynamicUpdate
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class STCEventEB extends AbstractEntity<Long> {

    @Id
    @SequenceGenerator(name = "EVENT_ID_SEQ", sequenceName = "EVENT_ID_SEQ", initialValue = 1000)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EVENT_ID_SEQ")
    private Long id;

    @Column(name = "EVENT_ID", nullable = false)
    private String eventId;

    @Type(type = "json-descriptor")
    @Column(name = "callback", length = 2000)
    private EventCallbackEB eventCallbackEB;

    @Override
    public Long pk() {
        return getId();
    }
}

