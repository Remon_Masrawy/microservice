package com.knet.imore.stc.service.dao;

import com.knet.imore.stc.service.model.STCSubscriptionEventEB;

public interface SubscriptionEventDAO extends AbstractDao<STCSubscriptionEventEB, Long> {
}
